/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien CALISTE, laboratoire L_Sim, (2001-2008)
  
	Adresse m�l :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors: Damien CALISTE, laboratoire L_Sim, (2001-2008)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at
        Documentation/licence.en.txt.
*/

#include "plane_py.h"

#include <pygobject.h>

static int       planePy_init   (VisuPlanePy *self, PyObject *args, PyObject *kwds);
static PyObject* planePy_new    (PyTypeObject *type, PyObject *args, PyObject *kwds);
static void      planePy_free   (VisuPlanePy* self);
static PyObject* planePy_set    (PyObject *obj, PyObject *args, PyObject *kwds);
static PyObject* planePy_getRGBA(PyObject *obj, PyObject *args);

static PyMethodDef VisuPlanePyMethods[] =
  {
    {"set", (PyCFunction)planePy_set, METH_VARARGS | METH_KEYWORDS,
     "Change the characteristics of a VisuPlane.\n\n"
     "Keyword arguments:\n"
     "rgba     (optional) -- a tuple of four floats values between 0 and 1\n"
     "                       describing the RGBA colour of the plane.\n"
     "normal   (optional) -- a tuple of three floats values representing the\n"
     "                       normal vector of the plane.\n"
     "distance (optional) -- a float value with the distance of the plane to\n"
     "                       the origin of the basis set.\n"
     "rendered (optional) -- a boolean to say if the plane is drawn or not.\n"
     "hide     (optional) -- an integer value taking -1, 0 or +1 values\n"
     "                       specifying the hiding effect of the plane. With\n"
     "                       a value of 0, there is no masking, with +1, nodes\n"
     "                       after the plane are hidden and with -1, the ones\n"
     "                       before the plane."},
    {"getRGBA", (PyCFunction)planePy_getRGBA, METH_NOARGS,
     "Retrieve the colour of a plane.\n\n"
     "Returns: a tuple of four floats values between 0 and 1\n"
     "         describing the RGBA colour of the plane."},
    {NULL, NULL, 0, NULL}
  };
static PyTypeObject VisuPlanePyType = {
  PyObject_HEAD_INIT(NULL)
  0,                         /*ob_size*/
  "v_sim.VisuPlanePy",           /*tp_name*/
  sizeof(VisuPlanePy),           /*tp_basicsize*/
  0,                         /*tp_itemsize*/
  (destructor)planePy_free,  /*tp_dealloc*/
  0,                         /*tp_print*/
  0,                         /*tp_getattr*/
  0,                         /*tp_setattr*/
  0,                         /*tp_compare*/
  0,                         /*tp_repr*/
  0,                         /*tp_as_number*/
  0,                         /*tp_as_sequence*/
  0,                         /*tp_as_mapping*/
  0,                         /*tp_hash */
  0,                         /*tp_call*/
  0,                         /*tp_str*/
  0,                         /*tp_getattro*/
  0,                         /*tp_setattro*/
  0,                         /*tp_as_buffer*/
  Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
  "Ploum",                   /* tp_doc */
  0,                         /* tp_traverse */
  0,                         /* tp_clear */
  0,                         /* tp_richcompare */
  0,                         /* tp_weaklistoffset */
  0,                         /* tp_iter */
  0,                         /* tp_iternext */
  VisuPlanePyMethods,            /* tp_methods */
  0,                         /* tp_members */
  0,                         /* tp_getset */
  0,                         /* tp_base */
  0,                         /* tp_dict */
  0,                         /* tp_descr_get */
  0,                         /* tp_descr_set */
  0,                         /* tp_dictoffset */
  (initproc)planePy_init,    /* tp_init */
  0,                         /* tp_alloc */
  planePy_new,               /* tp_new */
/*   0, */
/*   0, */
/*   0, */
/*   0, */
/*   0, */
/*   0, */
/*   0, */
/*   0 */
};



static int planePy_init(VisuPlanePy *self _U_, PyObject *args _U_, PyObject *kwds _U_)
{
  return 0;
}
static PyObject* planePy_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
  VisuPlanePy *self;

  self = (VisuPlanePy*)type->tp_alloc(type, 0);
  if (self != NULL)
    {
      DBG_fprintf(stderr, "Visu Python: new VisuPlane object %p.\n", self);
      self->obj = visu_plane_newUndefined();
      if (!self->obj)
	{
	  Py_DECREF(self);
	  return NULL;
	}
    }
  else
    return NULL;

  /* We call the set method. */
  if (!planePy_set((PyObject*)self, args, kwds))
    {
      Py_DECREF(self);
      return NULL;
    }

/*   Py_INCREF(self); */
  return (PyObject *)self;
}
static void planePy_free(VisuPlanePy* self)
{
  DBG_fprintf(stderr, "Visu Python: deallocate a VisuPlane object %p.\n", self);

  if (self->obj)
    g_object_unref(G_OBJECT(self->obj));

  self->ob_type->tp_free((PyObject*)self);
}
static PyObject* planePy_set(PyObject *obj, PyObject *args, PyObject *kwds)
{
  VisuPlanePy *self;
  static char *kwlist[] = {"rgba", "normal", "distance",
			   "rendered", "hide", NULL};
  PyObject *rgbPy = NULL, *vectPy = NULL, *rendered = Py_True;
  ToolColor *color;
  float rgb[4] = {-1.f, -1.f, -1.f, -1.f};
  float vect[3] = {-123456.f, -1.f, -1.f};
  float dist = -12345.f;
  int res, hide = -2;

  self = (VisuPlanePy*)obj;

  DBG_fprintf(stderr, "Visu Python: set values for VisuPlane %p object %p.\n",
	      self->obj, obj);

  if (! PyArg_ParseTupleAndKeywords(args, kwds, "|O!O!fO!i", kwlist,
				    &PyTuple_Type, &rgbPy,
				    &PyTuple_Type, &vectPy,
				    &dist,
				    &PyBool_Type, &rendered,
				    &hide))
    return NULL;
  if (rgbPy)
    {
      if (! PyArg_ParseTuple(rgbPy, "ffff", rgb, rgb + 1, rgb + 2, rgb + 3))
	return NULL;
    }
  if (vectPy)
    {
      if (! PyArg_ParseTuple(vectPy, "fff", vect, vect + 1, vect + 2))
	return NULL;
    }
  
  res = 0;
  if (rgb[0] >= 0.f)
    {
      color = tool_color_addFloatRGBA(rgb, NULL);
      res = visu_plane_setColor(self->obj, color) || res;
    }
  if (vect[0] != -123456.f)
    res = visu_plane_setNormalVector(self->obj, vect) || res;
  if (dist != -12345.f)
    res = visu_plane_setDistanceFromOrigin(self->obj, dist) || res;
  res = visu_plane_setRendered(self->obj, (rendered == Py_True)) || res;
  if (hide != -2)
    res = visu_plane_setHiddenState(self->obj, hide) || res;

  if (res)
    {
      Py_INCREF(Py_True);
      return Py_True;
    }
  else
    {
      Py_INCREF(Py_False);
      return Py_False;
    }
}

static PyObject* planePy_getRGBA(PyObject *obj, PyObject *args)
{
  VisuPlanePy *self;
  PyObject *ret;
  ToolColor *color;

  self = (VisuPlanePy*)obj;

  DBG_fprintf(stderr, "Visu Python: get RGBA for VisuPlane %p object %p.\n",
	      self->obj, obj);

  visu_plane_getColor(self->obj, &color);
  ret = Py_BuildValue("(ffff)", color->rgba[0], color->rgba[1],
		      color->rgba[2], color->rgba[3]);
  
  Py_INCREF(ret);
  return ret;
}

PyTypeObject* planePyGet_type()
{
  return &VisuPlanePyType;
}
