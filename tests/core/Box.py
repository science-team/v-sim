#!/usr/bin/env python

import unittest

import gi
gi.require_version('v_sim', '3.8')
from gi.repository import GLib, v_sim

import signals

class TestBox(unittest.TestCase):
    def setUp(self):
        super(TestBox, self).setUp()
        self.addTypeEqualityFunc(float, self.fuzzyFloat)

    def fuzzyFloat(self, a, b, msg = None):
        if abs(b-a) > 6e-8:
            raise self.failureException(msg)

    def _boundary(self, bc):
        # Creation
        box = v_sim.Box.new((1., 2., 3., 4., 5., 6.), bc)
        self.assertEqual(box.getBoundary(), bc)
        box = v_sim.Box.new_full((1., 2., 3., 4., 5., 6., 7., 8., 9.), bc)
        self.assertEqual(box.getBoundary(), bc)
        # Setter
        box = v_sim.Box.new((1., 2., 3., 4., 5., 6.), v_sim.BoxBoundaries.FREE)
        with signals.Listener(box, "notify::boundary") as boundary:
            self.assertEqual(boundary.triggered(), 0)
            self.assertEqual(box.setBoundary(bc), bc != v_sim.BoxBoundaries.FREE)
            self.assertEqual(box.getBoundary(), bc)
            self.assertEqual(boundary.triggered(), 1 if bc != v_sim.BoxBoundaries.FREE else 0)
            box.set_property("boundary", v_sim.BoxBoundaries.FREE)
            self.assertEqual(box.getBoundary(), v_sim.BoxBoundaries.FREE)
            self.assertEqual(boundary.triggered(), 2 if bc != v_sim.BoxBoundaries.FREE else 1)

    def test_boundaries(self):
        self._boundary(v_sim.BoxBoundaries.FREE)
        self._boundary(v_sim.BoxBoundaries.WIRE_X)
        self._boundary(v_sim.BoxBoundaries.WIRE_Y)
        self._boundary(v_sim.BoxBoundaries.WIRE_Z)
        self._boundary(v_sim.BoxBoundaries.SURFACE_XY)
        self._boundary(v_sim.BoxBoundaries.SURFACE_YZ)
        self._boundary(v_sim.BoxBoundaries.SURFACE_ZX)
        self._boundary(v_sim.BoxBoundaries.PERIODIC)


if __name__ == '__main__':
    unittest.main()
