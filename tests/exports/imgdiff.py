#!/usr/bin/env python

import sys
import numpy
from scipy import misc

rad = sys.argv[1]

ref = misc.imread(sys.argv[2]).astype(numpy.float32)[:,:,0:3]

for i in range(3, len(sys.argv)):
  img = misc.imread(sys.argv[i]).astype(numpy.float32)[:,:,0:3]
  ssd = numpy.sqrt((ref - img) ** 2)
  print "%15g %15g" % (ssd.ptp(), ssd.sum() / ssd.shape[0] / ssd.shape[1]),
  
  ssd *= 10.
  ssd.clip(0.,255.)
  misc.imsave(rad + ".diff%d.png" % (i - 3), ssd.astype(numpy.uint8))

print "# %s peak-to-peak average" % rad
