<?xml version="1.0" encoding="utf-8"?>

<ChangeLog logiciel="V_Sim" lang="en">

    <milestone version="3.8" date="(2020-07)">
	<intro>All changes leading to 3.8.x series.</intro>

	<entry titre="Development version 3.7.99-1">
	<li type="rendering">Can now adjust the axis size.</li>
	<li type="capability">Allow colorisation from command-ine using the name of a node property.</li>
	<li type="capability">Implement loading of multiple scalar fields from command-line.</li>
	<li type="capability">Add support to draw coloured maps on arbitrary surfaces, not just planes.</li>
	<li type="ui">Switch to Gtk+3 by default and introspection build by default.</li>
	<li type="capability">Scalar field loading is now asynchronous.</li>
	<li type="rendering">Use smooth transition when zooming or moving the camera.</li>
	<li type="capability">Add a rotation method to move a group of nodes.</li>
	<li type="capability">New plug-in to detect point group symmetries.</li>
	</entry>

	<entry titre="Development version 3.7.99-0">
	<li type="capability">Add a fragment property to nodes, allowing to give a label and an id to a group of nodes.</li>
	<li type="rendering">Draw half pair when there is a neighbour by periodicity.</li>
	<li type="rendering">Draw cylinder pairs coloured by elements with the radii ratio (previously was always a 50/50 ratio).</li>
	<li type="capability">Add unit length to pairs and element radii.</li>
	<li type="capability">Add a command-line option to apply geometry differences.</li>
	</entry>

    </milestone>

    <milestone version="3.7" date="(2013-09)">
	<intro>All changes leading to 3.7.x series.</intro>

	<entry titre="Corrections 3.7.1 | on 2014-04-09">
	<li>Correct an issue with pair rendering.</li>
	<li>Add missing annotation for GObject Introspection.</li>
	<li>Correct axes toggle in UI not working.</li>
	<li>Correct crash when opening a scalar field from UI.</li>
	<li>Correct CLI option --i-set not working.</li>
	<li>Add support for older Glib than 2.28.</li>
	</entry>

	<entry titre="Stable version 3.7.0">
	<li type="rendering">Draw black line around planes and at intersections.</li>
	<li type="rendering">Axes can be used to render the box basis-set and can be positioned anywhere.</li>
	<li type="rendering">Add a colour legend for the colourisation.</li>
	<li type="rendering">SVG output now clips its surface so that nodes partially inside are correctly rendered.</li>
	<li type="capability">Add user defined shades in the resource file.</li>
	<li type="fileFormat">Resource file can be saved as XML file and merged with other XML files handled by V_Sim, like plane definitions…</li>
	</entry>

	<entry titre="Development version 3.6.99-3">
	<li type="interface">The save resources dialog now propose the available .res files of the current directory as possibilities in the path completion.</li>
	<li type="interface">The PythonGI plug-in logs stdout and stderr of the Python output.</li>
	<li type="rendering">Make the wire colouring depending on length applied only to the selected link and not to all.</li>
	<li type="capability">Add a way to use other function than "lower than" to hide nodes on colourisation data.</li>
	</entry>

	<entry titre="Development version 3.6.99-2">
	<li type="capability">Full support of the introspection for the core of V_Sim and for some parts of the GUI.</li>
	<li type="interface">The PythonGI plug-in adds a panel to plot scalar field values along lines in the box using Matplotlib as rendering widget for curves.</li>
	<li type="fileFormat">Using GObject-Introspection, add a parser for CNT files from VASP.</li>
	</entry>

	<entry titre="Development version 3.6.99-1">
	<li type="interface">The new BigDFT plug-in adds a panel to control and visualise parameters specific to BigDFT like the wavelet grids, the memory consumption, the pseudo-potential parameters... This plug-in is compatible with the linear version of the code. It can also be used to start or monitor a distant BigDFT run, retrieving density, potentials and wavefunctions.</li>
	<li type="fileFormat">Add natively a YAML parser, following the specifications from BigDFT.</li>
	<li type="interface">Add key bindings ('Ctrl+v') on the rendering window to popup the orientation chooser.</li>
	<li type="interface">Save prefered cameras in the resource file.</li>
	<li type="interface">Add a search bar in the rendering window (Ctrl+f) to highlight nodes from their number.</li>
	<li type="capability">Partial support of a possibility to scale the background image with camera settings.</li>
	<li type="rendering">Add resource entries to position the legend of the coloured maps.</li>
	<li type="capability">Make the manual range of colourisation working per column and not for all as before.</li>
	</entry>

	<entry titre="Development version 3.6.99-0">
	<li type="fileFormat">Support the wavefunction file format of BigDFT thanks to a dedicated plug-in.</li>
	<li type="capability">Add the possibility to draw arrows for forces directly in the atomic rendering mode without switching to the spin mode.</li>
	<li type="fileFormat">Implement a support for forces in ASCII and xyz file format.</li>
	<li type="fileFormat">Add a plug-in to handle compressed files (tar.gz, tar.bz2, ...).</li>
	<li type="interface">Add key bindings ('n' and 'p') to load next or previous file from the browser list.</li>
	<li type="interface">Add an option in the interactive dialog to hide highlighted or non-highlighted nodes.</li>
	<li type="interface">Add support for recent files. Opened and saved files now appear in the recent list of Gtk file chooser.</li>
	<li type="capability">Add an option to colourise nodes only if being in the manual range.</li>
	</entry>

	</milestone>

	<milestone version="3.6" date="(2011-06)">
	<intro>All changes leading to 3.6.x series.</intro>

	<entry titre="Corrections 3.6.1 | on --">
	<li>Correct the missing texts in bitmap exportations.</li>
	<li>Modify preview loading in open dialog to avoid grabbing of current file in case of long loading times.</li>
	<li>Correct the id of nodes in measurement history and restrain to 6 values only.</li>
	<li>Correct the not working spin modulus scaling.</li>
	<li>Upgrade ABINIT plug-in for version 6.8.x.</li>
	<li>Correct a crashing bug after several reload of a file with measurements when changing the colour.</li>
	<li>Correct a memory corruption in the distance curve widget.</li>
	<li>Make the drag action work again.</li>
	<li>Correct pair build when using translations on command line.</li>
	<li>Correct browser exportation of multi dataset files.</li>
	<li>Correct the exportation crash with ATI proprietary drivers.</li>
	</entry>

	<entry titre="Development version 3.5.99-4">
	<li type="interface">Display coordinates either in cartesian or reduced (see the configure panel).</li>
	<li type="interface">Keep zoom adjustment between two files of different box size (see option in the geometry panel).</li>
	<li type="rendering">Modify the coloured map rendering to use adaptive mesh. The legend displays also the isoline values.</li>
	<li type="interface">Add keybindings for the camera settings in the rendering window.</li>
	<li type="fileFormat">Add partial support for densities in XSF file format.</li>
	<li type="fileFormat">Add an exportation in ABINIT file format for the crystal part.</li>
	<li type="rendering">Add a transparency option for the coloured maps.</li>
	</entry>

	<entry titre="Development version 3.5.99-3">
	<li type="capability">Begin to implement a support of GObject-Introspection, to allow scripting (Python, Javascript, ...).</li>
	<li type="interface">The ABINIT interactive dialog now support different tolsym values and output the list of symmetries with their names.</li>
	<li type="interface">Add a progress bar for file loading when the loading is too long. Add also the possibility to abort loading process.</li>
	<li type="interface">V_Sim respects now the icon theme specification of FreeDesktop.</li>
	<li type="fileFormat">Demonstrate the Python scripting with the implementation of CNT file format of VASP.</li>
	<li type="interface">Add keybindings for the rendering area (save, reload and open).</li>
	<li type="capability">Add the possibility to export to any OpenBabel fileformats in command-line.</li>
	<li type="rendering">Modify the pair drawing to be able to use a per pair drawing method. Now, cylinder and wire pairs are available at the same time.</li>
	</entry>

	<entry titre="Development version 3.5.99-2">
	<li type="capability">Add a calculation of g(r) and add a tab in the pair dialog to display it. Linear or log scale are available and filtering between kind of neighbours is also possible.</li>
	<li type="interface">Sort pairs by distance.</li>
	<li type="fileFormat">Export a geometry difference in ASCII files as a keyword.</li>
	<li type="fileFormat">Add a keyword in ASCII files to store the total energy value of a system. This energy can be used to colourise paths.</li>
	<li type="fileFormat">Expand the XML file format generated by V_Sim to store the paths.</li>
	<li type="interface">Use nicer message in the file browser when the loading is too long.</li>
	<li type="capability">Add a command-line option to clamp coloured map values.</li>
	<li type="capability">Add a full vectorial PDF export with Cairo.</li>
	<li type="capability">Add the fog support in exportation in vectorial formats.</li>
	<li type="capability">Add data colourisation support in exportation in vectorial formats.</li>
	</entry>

	<entry titre="Development version 3.5.99-1">
	<li type="fileFormat">Add a resource for the radius of the
	highlight mark.</li>
	<li type="rendering">Implement a legend showing the box lengths.</li>
	<li type="fileFormat">Implement an exportation in CIF (from
	OpenBabel) file format.</li>
	<li type="interface">Implement a way to add two surfaces in one
	click, with good ressources for scalar fileds describing a
	wavefunction.</li>
	<li type="interface">Remove the deprecated GTK curve in the pair
	dialog and replace it with the combobox for shades.</li>
	<li type="rendering">Add a path drawing tool.</li>
	<li type="interface">Rename the panel "box" to "geometry" and store all geometry-related functions there, i.e. translation, expansion, units, paths...</li>
	</entry>
        </milestone>

	<milestone version="3.5" date="(2009-10)">
	<intro>All changes leading to 3.5.x series.</intro>

	<entry titre="Corrections 3.5.3 | on --">
	<li>Correct a bug after changing the basis set with the upper limit of node id selectors.</li>
	<li>Correct the position of the atom in case of spin rendering.</li>
	<li>Correct compilation issues with GCC >= 4.4 (thanks to Thierry Thomas for his patch).</li>
	<li>Correct a crashing bug in cube file format, when loading a file with a wrong atomic number.</li>
	<li>Correct missing information on nodes after an image dump.</li>
	</entry>

	<entry titre="Corrections 3.5.2 | on 2010-09-22">
	<li>Correct a mistake in the legend when loading a new file with a
	varying number of nodes per element.</li>
	<li>Correct a crash with the command line exportation.</li>
	<li>Correct a wrong sort in the browser panel for multiset files.</li>
	<li>Improve the output of the changelog in the about dialog window.</li>
	<li>Correct mispositioned atoms in case of non-orthorhombic boxes in
	XSF files.</li>
	<li>Add the reduced coordinates in case of peridic XYZ files.</li>
	<li>Give a good position to the duplicated atoms for free BC.</li>
	<li>Correct a blocking loop in the export dialog in case of wrong
	type detection.</li>
	<li>Correctly associate colorisation to nodes in case of not
	sorted node in input file.</li>
	</entry>

	<entry titre="Corrections 3.5.1 | on 2010-02-26">
	<li>Correct several warnings, for the color selector widget, the
	rendering of text in the OpenGL area or the loading of XML value
	file.</li>
	<li>Correct a wrong exportation of wire width greater than 10.</li>
	<li>Avoid useless dialog when loading a value file with partial
	data.</li>
	<li>Correct a masking bug for surfaces when the plane usage is
	unchecked.</li>
	<li>Allow to parse XYZ files with additionnal columns different from a
	vibration file.</li>
	<li>Correct a bug not showing the warning dialog box in case of error when exporting in
	image.</li>
	<li>Correct a bug affecting the positions of atoms in Cube files when
	boxes are non-orthorombic.</li>
	<li>Correct a crashing bug when pressing 'r' key without any saved camera.</li>
	<li>Correct a bug for non orthorombic boxes using OpenBabel plug-in, thanks to patch sent by Atz Togo.</li>
	<li>Correct a bug in ASCII file parsing when the files come from Macintosh or Windows.</li>
	<li>Make the 'open with' action works under Windows.</li>
	</entry>

	<entry titre="Development version 3.5.0-rc1">
	<li type="fileFormat">Add a support for angles in the XML value files.</li>
	<li type="interface">The tooltips in the browser now show the name
	of previous and next directories.</li>
	<li type="capability">Draw a legend in SVG dump.</li>
	<li type="capability">Add a way to change the basis set by
	pointing nodes as new vertices.</li>
	<li type="fileFormat">Declare in the resource file all parameters
	for the geometry diff (shape and size of the arrows, thresholds...).</li>
	<li type="fileFormat">Take into acount the boundary conditions
	when choosing the "up" direction. In case of surface, the y axis
	becomes the up axis.</li>
	<li type="interface">Add a tab in the interactive dialog to show
	symmetry information, as calculated by ABINIT (space group,
	equivalent atoms...).</li>
	<li type="interface">Add a key shortcut to switch between windows,
	mapped on "home" key.</li>
	<li type="rendering">Correct the visuals glitches when drawing
	cylindrical pairs.</li>
	</entry>

	<entry titre="Development version 3.4.99-4">
	<li type="fileFormat">Add phonons to ASCII file format.</li>
	<li type="capability">Implement the phonon properties for solid,
	taking into account the imaginary parts of displacements and the
	q.r phase shift.</li>
	<li type="interface">Add a clickable icon in the rendering window
	to access the saved cameras.</li>
	</entry>

	<entry titre="Development version 3.4.99-3">
	<li type="capability">Implement a visualisation for the difference
	in positions using small arrows on nodes.</li>
	<li type="capability">Add a phonon representation with vibration
	or arrows.</li>
	<li type="capability">Add labels to nodes. These labels can be
	rendered as for the type or the node id for instance. They can be
	modified in the interactive dialog.</li>
	<li type="capability">Implement the colourisation according to the
	coordinates.</li>
	<li type="rendering">Add a visualisation for the angles.</li>
	<li type="fileFormat">Add the support for labels for nodes in
	ASCII files.</li>
	<li type="fileFormat">Add a boundary condition keyword for ASCII files.</li>
	<li type="fileFormat">Parse a phonon column in the XYZ files.</li>
	<li type="interface">Add 'previous' and 'next' buttons into the browser.</li>
	<li type="interface">Add a tab for the phonon representation,
	listing available modes and displaying some parameters.</li>
	<li type="interface">Modify the pick mouse buttons to easily access
	the measuring tool. Add also some button in the rendering window
	to set or unset measurements.</li>
	<li type="capability">Add a scaling factor as post-process for the
	colourise tab.</li>
	<li type="rendering">Implement a legend displaying a frame with
	the atom names, number and representation.</li>
	<li type="rendering">Add a torus representation, in addition to
	existing ellipsoid and so on.</li>
	</entry>

	<entry titre="Development version 3.4.99-2">
	<li type="capability">Add a hiding capability to the highlighted nodes.</li>
	<li type="capability">Implement the support for several coloured maps.</li>
	<li type="rendering">Make the edges of the map planes smooth and
	highlight with a black line.</li>
	<li type="interface">Add a drag and drop capability for file
	loading on the OpenGL area.</li>
	<li type="fileFormat">Modify the ASCII format to allow reduced
	coordinates and box definition based on vector lengths and
	angles.</li>
	<li type="capability">Create a PDF/SVG exportation for the
	coloured maps.</li>
	<li type="interface">Change the keys 's' and 'r' as a ring to save
	and restore the camera position, instead of saving only one
	value.</li>
	<li type="capability">Add an exportation to XYZ file format.</li>
	<li type="interface">Implement in the box subpanel a way to
	quickly switch distances between common units (bohr, angstroems...).</li>
	<li type="interface">In the geometry modification panel, add a
	duplication possibility for the listed nodes.</li>
	</entry>

	<entry titre="Development version 3.4.99-1">
	<li type="capability">Start the implementation of Python bindings for V_Sim. It is possible to open a rendering window and to load a file in it. It is also psossible to create a VisuData from scratch, creating elements and nodes by hand. The planes capability has also been ported to Python.</li>
	<li type="fileFormat">Code a plug-in for cube file support, commonly used by SIESTA and introduced by Gaussian.</li>
	<li type="interface">Suppress the "highlight" tab in the interactive dialog. Move this capability to the pick tab using the control key to toggle highlight. Rework the relations between highlighted nodes and selected nodes. The highlight status can be changed in the listed nodes.</li>
	<li type="interface">Add a column to print the date of files in the browser.</li>
	<li type="capability">Extend the XML plane file to support various value fields in V_Sim. It is possible now to store in this XML file, the picked distances, the highlighted nodes, the values of created iso-surfaces.</li>
	<li type="fileFormat">Add the support for D3 posi files (concatenation of D3 files, adapted for movies).</li>
	<li type="capability">Add non linear shades, logarithmic scale for zero-centred data and add the possibility to choose the isoline colour.</li>
	<li type="interface">Reorganise a bit the pair dialog layout (the filter is now over the treeview and the management of links has been moved on the right).</li>
	</entry>
	</milestone>

	<milestone version="3.4" date="(2008-06)">

	<intro>All changes leading to 3.4.x series.</intro>

	<entry titre="Corrections 3.4.4 | on 2009-06-03">
	<li>Correct the not working -d command line option when used in
	conjonction of the -u option.</li>
	<li>Correct different memory leaks in the reading of input files
	or in the drawing routines.</li>
	<li>Make the 'r' and 's' keys work again after the opening of a
	dialog from the rendering window.</li>
	<li>Correct a crashing bug when nodes with persistent distances
	are removed.</li>
	<li>Correct the automatic reload function that was broken in 3.4 series.</li>
	<li>Correct wrong pathes in the browser when UTF8 characters are
	used.</li>
	<li>Correct a jump of the camera in the interactive session when
	leaving pick mode with a right click and a drag.</li>
	<li>Stop the interactive session also when the interactive dialog
	is closed by the window manager.</li>
	<li>Disconnect signals for orientation chooser when the dialog is destroyed..</li>
	</entry>

	<entry titre="Corrections 3.4.3 | on 2009-02-16">
	<li>Solve a loading problem when using the filechooser in spin mode.</li>
	<li>Correct a lack of cylinder pair drawing under specific
	conditions.</li>
	<li>Correct some "assertion fails" error when loading new
	files.</li>
	<li>Make the behaviour of "all elements" selection in the element
	tab works for atomic parameters in spin rendering.</li>
	<li>Correct the tab character problem in ASCII files.</li>
        </entry>

	<entry titre="Corrections 3.4.2 | on 2008-12-08">
	<li>Correct a bug in the pair dialog when the sort is used.
	Modifications in the treeview are then inconsistent.</li>
	<li>Correct a bug that makes the rendering disappear in the walker mode.</li>
	<li>Correct a bug when expanding the nodes that could create several nodes at one place.</li>
	<li>Correct a bug when the selection is zero pixel wide.</li>
	<li>Correct a bug in the move interactive dialog that prints a wrong number of selected nodes.</li>
        <li>Correct  the error while reading the resources of cylinder pairs with the minimum value.</li>
        <li>Add the missing exportation of the stipple pattern for the wire pairs.</li>
        </entry>

	<entry titre="Corrections 3.4.1 | on 2008-08-29">
	<li>Correct several Critical Warnings in drawDataOnNode() in the interactive dialog window.</li>
	<li>Correct a crash in the interactive window when the current VisuData is deleted.</li>
	<li>Correct wrong permissions on the directory created in the quit dialog.</li>
	<li>Correct a bug in the subpanel menu.</li>
	<li>Change the way to find a module name from its file name.</li>
	<li>Correct a linking problem with variable in .h on MacOS.</li>
	<li>Avoid V_Sim crash when a pixmap exportation is required on X
	server using AIGLX and a version of GLX prior to 1.3.</li>
        </entry>

	<entry titre="Stable release 3.4.0">
	<li>3.4.0.1: correct a segfault in the Abinit plugin.</li>
	<li>3.4.0.2: correct a possible segfault when a VisuData object is freed.</li>
	<li>3.4.0.2: add an XML filter to the plane file dialog.</li>
	<li>3.4.0.2: correct a bug in the colour selection of surfaces.</li>
	<li>3.4.0.2: import the debian directory from official repository and
	update it for 3.4.0 (made for 3.3.3 by Debian).</li>
	<li>3.4.0.3: change the location of legal dir to data dir.</li>
	<li>3.4.0.3: only install the plugins related files when plugins
        are compiled. Also move the images to the pixmap dir.</li>
	</entry>

	<entry titre="Development version 3.4-rc2">
	<li>Add the axes in the SVG output.</li>
	<li>Correct bugs (preview widget, ABINIT bindings) and complete translation.</li>
	</entry>

	<entry titre="Development version 3.4-rc1">
	<li>Add the capability to draw bitmap pictures on the
	background.</li>
	<li>Change the zoom behaviour to none when the box is expanded.</li>
	</entry>

	<entry titre="Development version 3.3.99-4">
	<li>Add a rectangular selection in the pick window and enable group moving in the move window.</li>
	<li>Add iso-lines for the coloured map.</li>
	<li>Add the support of vectors (for forces or spin) in the XSF plug-in.</li>
	<li>Add a possibility to link with the ABINIT parser to read its
	input file. This depends on modifications not yet in ABINIT
	official releases. They may enter in ABINIT 5.6 series. The data
	sets are supported and when the geometry builder is used, the
	created box is shown. Current limitation is when nband must be
	computed since no pseudo-potential files are loaded and thus
	nelect is not known.</li>
	<li>Add a tool to automatically compute range distance for first neighbours.</li>
	<li>Implement the possibility to draw pattern wires for pairs and box.</li>
	</entry>

	<entry titre="Development version 3.3.99-3">
	<li>Early implementation of a plug-in adding XSF reading
	capabilities. Atomic positions are only available yet.</li>
	<li>The coloured map can now be drawn in logarithmic scale.</li>
	<li>The XYZ parser has been modified to allow to read animation files. The browser panel has been modified accordingly to show the different node sets in one file.</li>
	<li>Activate once again the stereo rendering (was disabled from version 3.0.0 because of lack of hardware for testing). The stereo parameters, like the eyes distance, can be tuned in the OpenGL panel.</li>
	<li>Add keyboard support in the rendering window in observe
	mode. Page-Up and Page-Down keys are used for zoom and
	perspective. Also add the 's' key for 'save current camera
	position'. Key 'r' is now used to restore this saved
	position.</li>
	<li>Add the possibility of a non-uniform mesh for the scalar-field
	representation.</li>
	<li>Basic support for SVG exportation of box and nodes.</li>
	</entry>

	<entry titre="Development version 3.3.99-2">
	<li>Allow changes of coordinates in the pick table (as for spin
	characteristics or colour data). Add a possibility to write information
	on selected or all nodes.</li>
	<li>Better integration of OpenBabel library, with the use of the atom
	symbol instead of the atom labels, also use the OpenBabel colour and
	radius when the atom is not known by V_Sim. Try to reproduce as much
	as possible the handling of pairs as described in the OpenBabel input
	files.</li>
	<li>Add a filter in the pairs dialog to show only pairs of interest
	(i.e. pairs containing one given element).</li>
	<li>Implement a way to duplicate the box following a fractional ratio
	in the case of periodic boxes. Adapt all capabilities to take that
	extension into account (extend planes and coloured maps, duplicate
	colourisation data, draw box extension...).</li>
	<li>Separate the box &amp; axes subpanel into two subpanels and move the
	translations from the interactive window to the new box subpanel. Also
	add in this subpanel the gestion of box duplication.</li>
	<li>Enable the box duplication on command-line and also the
	coloured map.</li>
	<li>Modify the geometry tab in the interactive window to allow to add
	or remove nodes.</li>
	<li>Add a support for GtkGlExt as an alternative to the built-in
	OpenGL widget.</li>
	<li>Add a preview in the open dialog box, displaying the box and
	the elements with the current camera position.</li>
	<li>Implement the 'smooth and edge' rendering mode, displaying a
	line around polygons.</li>
	<li>Make the hiding of surfaces by planes smooth, adding polygons
	to avoid edgy borders.</li>
	<li>Improve the parse speed of the browser and add a multiple
	matching for rendering method with several type files (spin and
	positions can be loaded in one clic).</li>
	<li>Use the XDG specification for the location of the home config
	directory. The old $HOME/.v_sim is still supported.</li>
	</entry>

	<entry titre="Development version 3.3.99-1">
	<li>Change the surface resources in the panel: each surfaces can
	has a name or not. In the latter case, the resources of the
	surface are private to this surface (all changes do not affect
	the other surfaces). Merge the two property dialogs into one
	(previously change current and change all). Add a special add
	button in the panel to add several surfaces at once (use the same
	than in the build dialog). Make the remove button remove all
	surfaces of a scalar field file before removing the file
	itself.</li>
	<li>Implement the masking effect of planes on surfaces with a
	simple implementation (not smooth borders).</li>
	<li>Make the masking effect of planes selective per surface or per
	element (see the new masking column in surface panel and the check
	box in the element panel).</li>
	<li>Add a capability to print all distance of a pair (see the pair
	dialog and the 'length column). Distance values are now editable
	in the treeview.</li>
	<li>Add an option in the browser panel to read the directories
	recursively. Also makes the double-click check the file entry in
	the list and implement the capability to browse several
	directories at one time.</li>
	<li>Add a new widget to choose the orientation, using an
	orthogonal basis set, the box coordinates or the spherical basis set.</li>
	</entry>
	</milestone>



	<milestone version="3.3" date="(2007-03)">

	<intro>All changes leading to 3.3.x series.</intro>

	<entry titre="Corrections 3.3.3 | on 2008-05-20">
	<li>Correct the implementation of the translation inside windows with a non-unitary ratio aspect.</li>
	<li>Correct a memory leak during image exportation with the XLib.</li>
	<li>Correct a memory leak in the save dialog window.</li>
        </entry>

	<entry titre="Corrections 3.3.2 | on 2008-04-01">
	<li>Add a man page.</li>
	<li>Use a correct format for the v_sim.ini file in Windows.</li>
	<li>Correct a bug making D3 file read impossible on Windows.</li>
	<li>Correct a bug when selected the root directory.</li>
        <li>Correct a crashing bug on MacOSX and Intel cards on Linux.</li>
        </entry>

	<entry titre="Corrections 3.3.1 | on 2007-11-15">
	<li>Make the normalisation of the colorisation stay during automatic reloading.</li>
	<li>Correct a bug in the coordinate conversion for non-orthogonal
	boxes (both in structure and scalar field).</li>
	<li>Correct a bug in D3 file parsing.</li>
	<li>Correct an internal error dealing with to much signal
	connection to the VisuData objects.</li>
	</entry>

	<entry titre="Development version 3.3.0-rc1">
	<li>Add a rendering shape as points for atoms to be able to plot grid position or to increase speed when a huge quantities of elements is drawn.</li>
	<li>Implement a binary format for the spin rendering.</li>
	<li>Some improvements in windows handling: it is possible to raise the command panel from the rendering window for instance.</li>
	<li>Small improvements and redesign in the save parameters and resources window: it is possible to export resources related to rendered object only.</li>
	<li>End of implemntation of separable subpanels. Their size and positions are now changeable and can be exported to the parameter file.</li>
	</entry>

	<entry titre="Development version 3.2.99-5">
	<li>Upgrade the build system to be able to build plugins for Windows.</li>
	<li>Change the transparency handling of isosurfaces to avoid to compute the order of them when the camera is moving.</li>
	<li>Several bug corrections, including error on reading resources related to isosurfaces, wrong computation of normal vector of isosurfaces, automatic reload of file...</li>
	<li>Improvements done on the gestion of the properties of isosurfaces. Names are editable and can be passed on command line when option -v is used. Change also the behavior of selecting buttons in that subpanel to be able to change properties of all surfaces of one unique file.</li>
	<li>Save in the plane file in XML the rendering state of each plane.</li>
	<li>Upgrade translation file for future version 3.3.</li>
	</entry>

	<entry titre="Development version 3.2.99-4">
	<li>Implementation for the reading of scalar fields encoding with the Nanoquanta specification v1.3. Only real scalar fields (i.e. densities) without spin informations are currently supported. From this point basic support for specification v1.3 is operational.</li>
	<li>Addition of a subpanel to draw colored planes representing the variations of a given scalar field. A colored scale is also available.</li>
	<li>Density files and surface files can be automatically loaded and all drawing capabilities are available from the command line.</li>
	<li>Colors and material values are more coherent between elements and surfaces.</li>
	</entry>

	<entry titre="Development version 3.2.99-3">
	<li>Commandline capabilities for new fonctionalities associated to iso-surfaces.</li>
	<li>Support of density/potential files directly in the isosurfaces panel, without need to use the convert tool. Implementation also of the merge of several surfaces directly in this panel.</li>
	<li>Adding remove and add of surfaces on the fly in the isosurfaces panel. The add action is available only for density/potential files.</li>
	<li>Complete support for detaching panels and associate them in several dialogs.</li>
	</entry>

	<entry titre="Development version 3.2.99-2">
	<li>Implementation of the modulus handling in spin rendering. Spin shapes can be scaled according to the modulus read in the spin input file.</li>
	<li>Total implementation of OpenBabel support. All format readable with OpenBabel are loadable in V_Sim.</li>
	<li>Creation of a tool to retrieve values attached to nodes, such as colorisation informations or orientation and modulus for spin rendering. In this former rendering mode, these value are editable on the fly.</li>
	<li>Some changements in the saving of resources of spin rendering. It is recommended to overwrite resources files since three keywords are now obsolete.</li>
	<li>Some interface enhancements.</li>
	</entry>

	<entry titre="Development version 3.2.99-1">
	<li>Adding a basic support for plugins. Shared libraries that matched the interfaces required by V_Sim located in the installation directory and in the user one (${HOME}/.v_sim) are automatically loaded on startup.</li>
	<li>Partial implemntation of NANOQUANTA v1.2 specifications for atomic positions (plugin).</li>
	<li>Usage of OpenBabel library for file loading (plugin), but nothing is actually functional.</li>
	<li>Basic fuunctionnality to detach tabs from the command panel.</li>
	<li>Creation of a simple OpenGL widget to avoid the hack used since v3.0 version to render OpenGL surfaces. Doing it, the backing store support has been removed and replaced by a complete gestion of the 'expose' events.</li>
	<li>Improvements in the spin rendering mode where it is now possible to choose between, always spin, hide spin with a null modulus or draw atomic shapes instead.</li>
	<li>Support of the atomic rendering within the spin one.</li>
	<li>Addition of an elipsoid shape for both spin and atomic rendering.</li>
	<li>Support for distance measurements in the default mode (restricted before to the pick / observe window).</li>
	</entry>
	</milestone>


	<milestone version="3.2" date="(2006-05)">

	<intro>All changes leading to 3.2.x series.</intro>

  <entry titre="Corrections 3.2.1 | on 2006-09-10">
  <li>Correct a reading bug in the convert dialog for isosurfaces, when a potential/density file is read.</li>
  <li>Correct a crashing bug using box and axes when no file is rendered.</li>
  <li>Correct a crashing bug in the isosurface panel, when playing a list with only one entry.</li>
  <li>Correct a crashing bug in the colorization panel.</li>
  <li>Correct a bug with 64bits machines that disables the camera rotation in the observe window.</li>
  <li>Correct a mispel in the French translation.</li>
  </entry>
  
	<entry titre="Development version 3.2.0-rc1">
	<li>A warning message dialog has been added when the quit button is clicked. This message is optional and can be hidden with a key in the config file named 'v_sim.par' or by checking a button in this window.</li>
	</entry>

	<entry titre="Development version 3.1.99-7">
	<li>Adding a functionnality to save or open informations about planes. The data type used is simple XML. The geometry, the hidden state and the color of each plane are stored. Only the drawn options is not saved.</li>
	<li>Implement the constrained movements along x, y and z axis in the geometry builder.</li>
	<li>Choose automatically the action associated to a tab when one is selected in the interactive window.</li>
	</entry>

	<entry titre="Development version 3.1.99-6">
	<li>Implementing a new action to move the nodes. Using drag and drop, it is possible to move nodes in the plane of the screen.</li>
	<li>Adding a ascii export with the option to output or not hidden nodes.</li>
	<li>A new tab has been added in the planes subpanel to allow to change automatically the distance from origin (as it is done in the browser subpanel to chow several files one after an other).</li>
	<li>Creating a new action to mark atoms. It draws some frame around clicked nodes to easily follow some.</li>
	</entry>

	<entry titre="Development version 3.1.99-5">
	<li>Internal modifications to suppress global pointers (especially in opengl.c and visu_data.c).</li>
	<li>VisuData is switched to a GObject and signals about OpenGL, Nodes and Elements are moved from visu_object to it.</li>
	</entry>

	<entry titre="Development version 3.1.99-4">
	<li>Through the selection dialog, distances that are picked are label on screen and stay printed as long as required.</li>
	<li>Add a possibility to hide some nodes in the colorization subpanel, depending on values in the associated data file.</li>
	<li>Enable the choice of rendering mode per drawn elements, e.g. atoms are drawn smoothly wereas isosurfaces are rendered through wireframe (requires GTK+2.6).</li>
	<li>Add a custom value to tune the power of used lights and two preset values for lights : one with the defaukt light and another with four lights, customized for spin rendering.</li>
	<li>Adding a cycling mode in the browser. It is now possible to cycle once, to go on and back and still the classic cycle in loop.</li>
	<li>Rebuild of the configuration files dialog: switching to GtkComboBoxes and adding auto-completion.</li>
	<li>Improve French translation.</li>
	<li>Extend minimal pick behavior (i.e. default mode) to allow distance measurements.</li>
	<li>In pick/observe session, the little marks that identify references in pick mode, are now persistant during observe mode until the pick/observe session is closed.</li>
	<li>Correct bugs 2 and 75.</li>
	</entry>

	<entry titre="Development version 3.1.99-3">
        <li>The X11 backend mixed with the GTK interface is not supported anymore.</li>
        <li>Modify the main interface : buttons related to the rendered file moved to the bottom bar  on the rendering window.</li>
	<li>Switch to a GtkComboBox for the list to select subpanel.</li>
	<li>Update French translation.</li>
	<li>Adding a status bar in the rendering window. This allows to show some basic informations as the description of the rendered file, the number of drawn nodes... This is only working if the rendering backend of this window is GTK (not X11).</li>
	<li>Adding a minimal pick function on the third button as a default mode.</li>
	</entry>

	<entry titre="Development version 3.1.99-2">
	<li>GUI improvements in the browser sub-panel and display of currently browse directory.</li>
	<li>Adding JPG and PNG export image format, using GdkPixbuf.</li>
	<li>GUI changings of the dump dialog to allow to tune some parameters for different file formats (e.g. the compression ratio in JPEG format). Adding the possibility to choose the export size for images and to add automatically the file extension if not is specified.</li>
	</entry>
	
	<entry titre="Development version 3.1.99-1">
	<li>Changing default behavior (previously no action) to observe mode.</li>
	<li>Adding a check box in the dataFile sub-panel to automatically load data file with the same name than the one of the rendered file.</li>
	<li>Adding some preset shades in the dataFile sub-panel.</li>
	<li>Allowing the possibility to use dataFile methods through the command line.</li>
	<li>Displaying the color scale (in the dataFile sub-panel) even when several channels are changing if only one column is read.</li>
	<li>Adding the choice of masquing algorithm in the plane sub-panel (using union or intersection).</li>
	<li>Adding the possibility to specify in the command line some box translation.</li>
	</entry>

	</milestone>

	<milestone version="3.1" date="(2005-09)">

	<intro>Not translated in english.</intro>

	</milestone>

	<milestone version="3.0" date="(2005-03)">

	<intro>Not translated in english.</intro>

	</milestone>

</ChangeLog>
