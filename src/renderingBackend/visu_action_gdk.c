/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#define V_SIM_GDK
#include "visu_actionInterface.h"

#include <gdk/gdkkeysyms.h>

#include <gtk/gtk.h>
#include <support.h>

/**
 * tool_simplified_events_new_fromGdk:
 * @ev: a #ToolSimplifiedEvents structure.
 * @event: an incoming #GdkEvent structure.
 *
 * Transfer the given @event into the internal @ev structure.
 *
 * Since: 3.7
 *
 * Returns: if the event @ev is captured or not.
 **/
gboolean tool_simplified_events_new_fromGdk(ToolSimplifiedEvents *ev, const GdkEvent *event)
{
  g_return_val_if_fail(ev && event, FALSE);

  ev->button = 0;
  ev->buttonType = TOOL_BUTTON_TYPE_NONE;
  ev->motion = FALSE;
  ev->letter = '\0';
  ev->specialKey = Key_None;
  switch (event->type)
    {
    case (GDK_BUTTON_PRESS):
    case (GDK_BUTTON_RELEASE):
      /* case (GDK_DOUBLE_BUTTON_PRESS): */
      /* case (GDK_TRIPLE_BUTTON_PRESS): */
      ev->x = event->button.x;
      ev->y = event->button.y;
      ev->button = event->button.button;
      ev->shiftMod = event->button.state & GDK_SHIFT_MASK;
      ev->controlMod = event->button.state & GDK_CONTROL_MASK;
      if (event->type == GDK_BUTTON_PRESS)
        ev->buttonType = TOOL_BUTTON_TYPE_PRESS;
      else if (event->type == GDK_BUTTON_RELEASE)
        ev->buttonType = TOOL_BUTTON_TYPE_RELEASE;
      return TRUE;
    case (GDK_SCROLL):
      ev->x = event->scroll.x;
      ev->y = event->scroll.y;
      if (event->scroll.direction == GDK_SCROLL_UP)
        ev->button = 4;
      else if (event->scroll.direction == GDK_SCROLL_DOWN)
        ev->button = 5;
      ev->shiftMod = event->scroll.state & GDK_SHIFT_MASK;
      ev->controlMod = event->scroll.state & GDK_CONTROL_MASK;
      return TRUE;
    case (GDK_MOTION_NOTIFY):
      ev->motion = 1;
      ev->x = event->motion.x;
      ev->y = event->motion.y;
      gdk_window_get_root_coords(event->motion.window, ev->x, ev->y, &ev->root_x, &ev->root_y);
      if (event->motion.state & GDK_BUTTON1_MASK)
        ev->button = 1;
      else if (event->motion.state & GDK_BUTTON2_MASK)
        ev->button = 2;
      else if (event->motion.state & GDK_BUTTON3_MASK)
        ev->button = 3;
      ev->buttonType = 0;
      ev->shiftMod = event->motion.state & GDK_SHIFT_MASK;
      ev->controlMod = event->motion.state & GDK_CONTROL_MASK;
      if (event->motion.is_hint)
        {
#if GDK_MAJOR_VERSION > 2 || GDK_MINOR_VERSION > 11
          gdk_event_request_motions(&event->motion);
#else
          gdk_window_get_pointer(event->motion.window, NULL, NULL, NULL);
#endif
        }
      return TRUE;
    case(GDK_KEY_PRESS):
      if ((event->key.keyval == GDK_KEY_r || event->key.keyval == GDK_KEY_R) &&
          !(event->key.state & GDK_CONTROL_MASK))
        ev->letter = 'r';
      else if ((event->key.keyval == GDK_KEY_s || event->key.keyval == GDK_KEY_S) &&
               !(event->key.state & GDK_CONTROL_MASK))
        ev->letter = 's';
      else if ((event->key.keyval == GDK_KEY_x || event->key.keyval == GDK_KEY_X) &&
               !(event->key.state & GDK_CONTROL_MASK))
        ev->letter = 'x';
      else if ((event->key.keyval == GDK_KEY_y || event->key.keyval == GDK_KEY_Y) &&
               !(event->key.state & GDK_CONTROL_MASK))
        ev->letter = 'y';
      else if ((event->key.keyval == GDK_KEY_z || event->key.keyval == GDK_KEY_Z) &&
               !(event->key.state & GDK_CONTROL_MASK))
        ev->letter = 'z';
      /* else if(event->key.keyval == GDK_KEY_space) */
      /*   ev->letter = ' '; */
      else if(event->key.keyval == GDK_KEY_Page_Up)
        ev->specialKey = Key_Page_Up;
      else if(event->key.keyval == GDK_KEY_Page_Down)
        ev->specialKey = Key_Page_Down;
      else if(event->key.keyval == GDK_KEY_Down)
        ev->specialKey = Key_Arrow_Down;
      else if(event->key.keyval == GDK_KEY_Up)
        ev->specialKey = Key_Arrow_Up;
      else if(event->key.keyval == GDK_KEY_Left)
        ev->specialKey = Key_Arrow_Left;
      else if(event->key.keyval == GDK_KEY_Right)
        ev->specialKey = Key_Arrow_Right;
#if GDK_MAJOR_VERSION > 2
      else if(event->key.keyval == GDK_KEY_Menu)
        {
          gdk_window_get_device_position(event->key.window,
#if GDK_MAJOR_VERSION < 3 || (GDK_MAJOR_VERSION == 3 && GDK_MINOR_VERSION < 20)
                                         gdk_device_manager_get_client_pointer
                                         (gdk_display_get_device_manager
                                          (gdk_window_get_display(event->key.window))),
#else
                                         gdk_seat_get_pointer(gdk_event_get_seat(event)),
#endif
                                         &ev->x, &ev->y, NULL);
          gdk_window_get_root_coords(event->key.window,
                                     ev->x, ev->y, &ev->root_x, &ev->root_y);
          ev->specialKey = Key_Menu;
        }
#endif
      ev->shiftMod = event->key.state & GDK_SHIFT_MASK;
      ev->controlMod = event->key.state & GDK_CONTROL_MASK;
      return TRUE;
    case(GDK_KEY_RELEASE):
      return TRUE;
    default:
      return FALSE;
    }
  return TRUE;
}
