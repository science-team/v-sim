/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_DATA_LOADABLE_H
#define VISU_DATA_LOADABLE_H

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include "visu_data.h"

G_BEGIN_DECLS

#define VISU_TYPE_DATA_LOADABLE         (visu_data_loadable_get_type ())
#define VISU_DATA_LOADABLE(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_TYPE_DATA_LOADABLE, VisuDataLoadable))
#define VISU_DATA_LOADABLE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), VISU_TYPE_DATA_LOADABLE, VisuDataLoadableClass))
#define VISU_IS_DATA_LOADABLE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_TYPE_DATA_LOADABLE))
#define VISU_IS_DATA_LOADABLE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), VISU_TYPE_DATA_LOADABLE))
#define VISU_DATA_LOADABLE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_DATA_LOADABLE, VisuDataLoadableClass))

typedef struct _VisuDataLoadable VisuDataLoadable;
typedef struct _VisuDataLoadablePrivate VisuDataLoadablePrivate;

struct _VisuDataLoadable
{
  VisuData parent;

  VisuDataLoadablePrivate *priv;
};

typedef struct _VisuDataLoadableClass VisuDataLoadableClass;

/**
 * VisuDataLoadableClass:
 * @parent: the parent class.
 * @getFilename: a method used to get the filename associated to
 * specific type.
 * @load: the loading method.
 *
 * The #VisuDataLoadableClass definition.
 *
 * Since: 3.8
 */
struct _VisuDataLoadableClass
{
  VisuDataClass parent;

  const gchar* (*getFilename) (const VisuDataLoadable *self, guint type);
  gboolean (*load) (VisuDataLoadable *self, guint iSet,
                    GCancellable *cancel, GError **error);
};

GType visu_data_loadable_get_type (void);

/**
 * VisuDataLoadableErrorFlag:
 * @DATA_LOADABLE_ERROR_METHOD: Error from the #VisuDataLoadable method.
 * @DATA_LOADABLE_ERROR_FILE: Error when opening.
 * @DATA_LOADABLE_ERROR_FORMAT: Wrongness in format.
 * @DATA_LOADABLE_ERROR_UNKNOWN: an error without specification.
 * @DATA_LOADABLE_ERROR_CANCEL: the #VisuDataLoadable operation has been cancelled.
 *
 * Thiese are flags used when reading a file with a #VisuDataLoadable method.
 */
typedef enum
  {
    DATA_LOADABLE_ERROR_METHOD,
    DATA_LOADABLE_ERROR_FILE,
    DATA_LOADABLE_ERROR_FORMAT,
    DATA_LOADABLE_ERROR_UNKNOWN,
    DATA_LOADABLE_ERROR_CANCEL
  } VisuDataLoadableErrorFlag;

/**
 * VISU_DATA_LOADABLE_ERROR:
 *
 * Domain of error for #VisuDataLoadable objects.
 */
#define VISU_DATA_LOADABLE_ERROR visu_data_loadable_getErrorQuark()
GQuark visu_data_loadable_getErrorQuark(void);

VisuDataLoadable* visu_data_loadable_new_fromCLI(void);
gboolean visu_data_loadable_checkFile(VisuDataLoadable *self,
                                      guint fileType, GError **error);

const gchar* visu_data_loadable_getFilename(const VisuDataLoadable *self, guint type);
gboolean visu_data_loadable_load(VisuDataLoadable *self, guint iSet,
                                 GCancellable *cancel, GError **error);
gboolean visu_data_loadable_reload(VisuDataLoadable *self,
                                   GCancellable *cancel, GError **error);
guint visu_data_loadable_getSetId(const VisuDataLoadable *self);
gboolean visu_data_loadable_setNSets(VisuDataLoadable *self, guint nSets);
guint visu_data_loadable_getNSets(const VisuDataLoadable *self);

const gchar* visu_data_loadable_getSetLabel(const VisuDataLoadable *self, guint iSet);
void visu_data_loadable_setSetLabel(VisuDataLoadable *self, const gchar* label, guint iSet);

G_END_DECLS

#endif
