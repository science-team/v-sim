/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef IFACE_ANIMATABLE_H
#define IFACE_ANIMATABLE_H

#include <glib.h>
#include <glib-object.h>

#include "visu_animation.h"

G_BEGIN_DECLS

/* Animatable interface. */
#define VISU_TYPE_ANIMATABLE                (visu_animatable_get_type ())
#define VISU_ANIMATABLE(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_TYPE_ANIMATABLE, VisuAnimatable))
#define VISU_IS_ANIMATABLE(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_TYPE_ANIMATABLE))
#define VISU_ANIMATABLE_GET_INTERFACE(inst) (G_TYPE_INSTANCE_GET_INTERFACE ((inst), VISU_TYPE_ANIMATABLE, VisuAnimatableInterface))

typedef struct _VisuAnimatableInterface VisuAnimatableInterface;
typedef struct _VisuAnimatable VisuAnimatable; /* dummy object */

/**
 * VisuAnimatable:
 *
 * Interface object.
 *
 * Since: 3.8
 */

/**
 * VisuAnimatableInterface:
 * @parent: its parent.
 * @get_animation: a method retrieve the #VisuAnimation object of the
 * given property.
 *
 * The different routines common to objects implementing a #VisuAnimatable interface.
 * 
 * Since: 3.8
 */
struct _VisuAnimatableInterface
{
  GTypeInterface parent;

  VisuAnimation* (*get_animation)(const VisuAnimatable *self, const gchar *prop);
};

GType visu_animatable_get_type(void);

VisuAnimation* visu_animatable_getAnimation(const VisuAnimatable *animatable,
                                            const gchar *prop);
gboolean visu_animatable_animate(VisuAnimatable *animatable, VisuAnimation *anim,
                                 const GValue *to, gulong duration, gboolean loop, VisuAnimationType type);

gboolean visu_animatable_animateFloat(VisuAnimatable *animatable, VisuAnimation *anim,
                                      float to, gulong duration, gboolean loop, VisuAnimationType type);
gboolean visu_animatable_animateFloatByName(VisuAnimatable *animatable, const gchar *prop,
                                            float to, gulong duration, gboolean loop, VisuAnimationType type);

gboolean visu_animatable_animateDouble(VisuAnimatable *animatable, VisuAnimation *anim,
                                       double to, gulong duration, gboolean loop, VisuAnimationType type);
gboolean visu_animatable_animateDoubleByName(VisuAnimatable *animatable, const gchar *prop,
                                             double to, gulong duration, gboolean loop, VisuAnimationType type);

G_END_DECLS

#endif
