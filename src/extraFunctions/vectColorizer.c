/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2018)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2018)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "vectColorizer.h"

/**
 * SECTION:vectColorizer
 * @short_description: A class defining node colorisation according to
 * #VisuNodeValuesVector.
 *
 * <para>This class implements #VisuDataColorizer for data coming from
 * #VisuNodeValuesVector. The colorisation is done according to vector
 * norm.</para>
 */

static gboolean _toValues(const VisuDataColorizerShaded *colorizer,
                          gfloat values[3], const VisuData *visuData,
                          const VisuNode* node);

G_DEFINE_TYPE(VisuDataColorizerVector, visu_data_colorizer_vector,
              VISU_TYPE_DATA_COLORIZER_SHADED)

static void visu_data_colorizer_vector_class_init(VisuDataColorizerVectorClass *klass)
{
  /* Connect the overloading methods. */
  VISU_DATA_COLORIZER_SHADED_CLASS(klass)->toValues = _toValues;
}

static void visu_data_colorizer_vector_init(VisuDataColorizerVector *obj _U_)
{
}

/**
 * visu_data_colorizer_vector_new:
 *
 * Creates a #VisuDataColorizer object to colorize and hide #VisuNode
 * based on a #VisuNodeValuesVector model.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a newly created
 * #VisuDataColorizerVector object.
 **/
VisuDataColorizerVector* visu_data_colorizer_vector_new()
{
  return VISU_DATA_COLORIZER_VECTOR(g_object_new(VISU_TYPE_DATA_COLORIZER_VECTOR, NULL));
}

/**
 * visu_data_colorizer_vector_setNodeModel:
 * @colorizer: a #VisuDataColorizerVector object.
 * @model: (transfer none): a #VisuNodeValuesVector object.
 *
 * Associate @model to @colorizer.
 *
 * Since: 3.8
 *
 * Returns: TRUE if the model is indeed changed.
 **/
gboolean visu_data_colorizer_vector_setNodeModel(VisuDataColorizerVector *colorizer,
                                                 VisuNodeValuesVector *model)
{
  return visu_sourceable_setNodeModel(VISU_SOURCEABLE(colorizer),
                                      VISU_NODE_VALUES(model));
}

static gboolean _toValues(const VisuDataColorizerShaded *colorizer,
                          gfloat values[3], const VisuData *visuData _U_,
                          const VisuNode* node)
{
  const gfloat *f;
  const VisuNodeValues *model;
  gfloat m, M, v;

  model = visu_sourceable_getConstNodeModel(VISU_SOURCEABLE(colorizer));
  f = visu_node_values_vector_getAtSpherical(VISU_NODE_VALUES_VECTOR(model), node);
  if (!f)
    return FALSE;
  m = visu_node_values_farray_min(VISU_NODE_VALUES_FARRAY(model));
  M = visu_node_values_farray_max(VISU_NODE_VALUES_FARRAY(model));

  v = (f[TOOL_MATRIX_SPHERICAL_MODULUS] - m) / (M - m);
  values[0] = values[1] = values[2] = v;
  return TRUE;
}
