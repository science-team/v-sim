/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "stringProp.h"
#include <string.h>

/**
 * SECTION:stringProp
 * @short_description: define a #VisuNodeValues object to handle strings.
 *
 * <para>Defines a #VisuNodeValues object to store strings on every
 * nodes and get notification for them.</para>
 */

static gboolean _parse(VisuNodeValues *vals, VisuNode *node, const gchar *from);
static gchar* _serialize(const VisuNodeValues *vals, const VisuNode *node);

G_DEFINE_TYPE(VisuNodeValuesString, visu_node_values_string, VISU_TYPE_NODE_VALUES)

static void visu_node_values_string_class_init(VisuNodeValuesStringClass *klass)
{
  /* Connect the overloading methods. */
  VISU_NODE_VALUES_CLASS(klass)->parse = _parse;
  VISU_NODE_VALUES_CLASS(klass)->serialize = _serialize;
}

static void visu_node_values_string_init(VisuNodeValuesString *self _U_)
{
}

static gboolean _parse(VisuNodeValues *vals, VisuNode *node, const gchar *from)
{
  return visu_node_values_string_setAt(VISU_NODE_VALUES_STRING(vals), node, from);
}
static gchar* _serialize(const VisuNodeValues *vals, const VisuNode *node)
{
  GValue value = {0, {{0}, {0}}};

  if (!visu_node_values_getAt(vals, node, &value))
    return (gchar*)0;

  return g_value_dup_string(&value);
}

/**
 * visu_node_values_string_new:
 * @arr: a #VisuNodeArray object.
 * @label: a translatable label.
 *
 * Create a new string field located on nodes.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a newly created #VisuNodeValuesString object.
 **/
VisuNodeValuesString* visu_node_values_string_new(VisuNodeArray *arr,
                                                  const gchar *label)
{
  VisuNodeValuesString *vals;

  vals = VISU_NODE_VALUES_STRING(g_object_new(VISU_TYPE_NODE_VALUES_STRING,
                                              "nodes", arr, "label", label,
                                              "type", G_TYPE_STRING, NULL));
  return vals;
}

/**
 * visu_node_values_string_getAt:
 * @vect: a #VisuNodeValuesString object.
 * @node: a #VisuNode object.
 *
 * Retrieves the float array hosted on @node.
 *
 * Since: 3.8
 *
 * Returns: (transfer none): the coordinates of
 * float array for @node.
 **/
const gchar* visu_node_values_string_getAt(VisuNodeValuesString *vect,
                                            const VisuNode *node)
{
  GValue value = {0, {{0}, {0}}};

  g_return_val_if_fail(VISU_IS_NODE_VALUES_STRING(vect), NULL);

  visu_node_values_getAt(VISU_NODE_VALUES(vect), node, &value);
  return g_value_get_string(&value);
}

/**
 * visu_node_values_string_setAt:
 * @vect: a #VisuNodeValuesString object.
 * @node: a #VisuNode object.
 * @str: string value.
 *
 * Changes the string hosted at @node for one of defined by @str.
 *
 * Since: 3.8
 *
 * Returns: TRUE if string for @node is indeed changed.
 **/
gboolean visu_node_values_string_setAt(VisuNodeValuesString *vect,
                                       const VisuNode *node,
                                       const gchar *str)
{
  GValue value = {0, {{0}, {0}}};

  visu_node_values_getAt(VISU_NODE_VALUES(vect), node, &value);
  if (str && g_value_get_string(&value) &&
      !strcmp(str, g_value_get_string(&value)))
    return FALSE;

  g_value_set_static_string(&value, str);
  return visu_node_values_setAt(VISU_NODE_VALUES(vect), node, &value);
}
