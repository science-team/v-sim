/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2019)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2019)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "shadedColorizer.h"

/**
 * SECTION:shadedColorizer
 * @short_description: A class defining node colorisation using
 * a #ToolShade.
 *
 * <para>This class implements #VisuDataColorizer and computes the
 * colors to apply to nodes from a #ToolShade.</para>
 */

enum
  {
    PROP_0,
    SHADE_PROP,
    N_PROP
  };
static GParamSpec *properties[N_PROP];

struct _VisuDataColorizerShadedPrivate
{
  ToolShade *shade;
};

static void visu_data_colorizer_shaded_finalize(GObject* obj);
static void visu_data_colorizer_shaded_get_property(GObject* obj, guint property_id,
                                                    GValue *value, GParamSpec *pspec);
static void visu_data_colorizer_shaded_set_property(GObject* obj, guint property_id,
                                                    const GValue *value, GParamSpec *pspec);
static gboolean _colorize(const VisuDataColorizer *colorizer,
                          float rgba[4], const VisuData *visuData,
                          const VisuNode* node);

G_DEFINE_TYPE_WITH_CODE(VisuDataColorizerShaded, visu_data_colorizer_shaded,
                        VISU_TYPE_DATA_COLORIZER,
                        G_ADD_PRIVATE(VisuDataColorizerShaded))

static void visu_data_colorizer_shaded_class_init(VisuDataColorizerShadedClass *klass)
{
  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->finalize = visu_data_colorizer_shaded_finalize;
  G_OBJECT_CLASS(klass)->set_property = visu_data_colorizer_shaded_set_property;
  G_OBJECT_CLASS(klass)->get_property = visu_data_colorizer_shaded_get_property;
  VISU_DATA_COLORIZER_CLASS(klass)->colorize = _colorize;

  /**
   * VisuColorization::shade:
   *
   * The shading used to colorise.
   *
   * Since: 3.8
   */
  properties[SHADE_PROP] = g_param_spec_boxed("shade", "Shade",
                                              "shading scheme",
                                              TOOL_TYPE_SHADE,
                                              G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);
  g_object_class_install_properties(G_OBJECT_CLASS(klass), N_PROP, properties);
}

static void visu_data_colorizer_shaded_get_property(GObject* obj, guint property_id,
                                                    GValue *value, GParamSpec *pspec)
{
  VisuDataColorizerShadedPrivate *self = VISU_DATA_COLORIZER_SHADED(obj)->priv;

  DBG_fprintf(stderr, "Colorizer Shaded: get property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case SHADE_PROP:
      g_value_set_boxed(value, self->shade);
      DBG_fprintf(stderr, "%p.\n", (gpointer)self->shade);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_data_colorizer_shaded_set_property(GObject* obj, guint property_id,
                                                    const GValue *value, GParamSpec *pspec)
{
  VisuDataColorizerShaded *self = VISU_DATA_COLORIZER_SHADED(obj);
  ToolShade *shade;

  DBG_fprintf(stderr, "Colorizer Shaded: set property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case SHADE_PROP:
      shade = (ToolShade*)g_value_get_boxed(value);
      DBG_fprintf(stderr, "%p.\n", (gpointer)shade);
      if (shade)
        visu_data_colorizer_shaded_setShade(self, shade);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}

static void visu_data_colorizer_shaded_init(VisuDataColorizerShaded *obj)
{
  DBG_fprintf(stderr, "Visu Data Colorizer Shaded: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = visu_data_colorizer_shaded_get_instance_private(obj);
  obj->priv->shade = tool_shade_copy(tool_shade_getById(0));
}

static void visu_data_colorizer_shaded_finalize(GObject *obj)
{
  VisuDataColorizerShaded *vect;

  g_return_if_fail(obj);

  vect = VISU_DATA_COLORIZER_SHADED(obj);

  tool_shade_free(vect->priv->shade);

  G_OBJECT_CLASS(visu_data_colorizer_shaded_parent_class)->finalize(obj);
}

/**
 * visu_data_colorizer_shaded_setShade:
 * @colorizer: a #VisuDataColorizerShaded object.
 * @shade: a #ToolShade object.
 *
 * Change the @shade used by the @colorizer.
 *
 * Since: 3.8
 *
 * Returns: TRUE if the shade is actually changed.
 **/
gboolean visu_data_colorizer_shaded_setShade(VisuDataColorizerShaded *colorizer,
                                             const ToolShade *shade)
{
  g_return_val_if_fail(VISU_IS_DATA_COLORIZER_SHADED(colorizer), FALSE);

  if (tool_shade_compare(colorizer->priv->shade, shade))
    return FALSE;

  tool_shade_free(colorizer->priv->shade);
  colorizer->priv->shade = tool_shade_copy(shade);

  g_object_notify_by_pspec(G_OBJECT(colorizer), properties[SHADE_PROP]);
  visu_data_colorizer_setDirty(VISU_DATA_COLORIZER(colorizer));

  return TRUE;
}
/**
 * visu_data_colorizer_shaded_getShade:
 * @colorizer: a #VisuDataColorizerShaded object.
 *
 * Return the shade used to colourise the nodes.
 *
 * Returns: (transfer none): the ToolShade used (own by V_Sim).
 */
const ToolShade* visu_data_colorizer_shaded_getShade(const VisuDataColorizerShaded *colorizer)
{
  g_return_val_if_fail(VISU_IS_DATA_COLORIZER_SHADED(colorizer), (const ToolShade*)0);

  return colorizer->priv->shade;
}

static gboolean _colorize(const VisuDataColorizer *colorizer,
                          float rgba[4], const VisuData *visuData,
                          const VisuNode* node)
{
  VisuDataColorizerShaded *self;
  VisuDataColorizerShadedClass *klass;
  gfloat values[3];
  
  g_return_val_if_fail(VISU_IS_DATA_COLORIZER_SHADED(colorizer), FALSE);

  self = VISU_DATA_COLORIZER_SHADED(colorizer);
  klass = VISU_DATA_COLORIZER_SHADED_GET_CLASS(self);
  if (!klass->toValues)
    return FALSE;
  if (klass->toValues(self, values, visuData, node))
    tool_shade_channelToRGB(self->priv->shade, rgba, values);
  else
    return FALSE;
  return TRUE;
}
