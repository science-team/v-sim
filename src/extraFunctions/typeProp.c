/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "typeProp.h"
#include <coreTools/toolMatrix.h>

#include <math.h>

/**
 * SECTION:typeProp
 * @short_description: define a #VisuNodeValues object to access node typeinates.
 *
 * <para>Defines a #VisuNodeValues object to be notified about
 * typeinate changes.</para>
 */


static gboolean _getAt(const VisuNodeValues *vals, const VisuNode *node,
                       GValue *value);
static gboolean _setAt(VisuNodeValues *vals, const VisuNode *node,
                       GValue *value);
static gboolean _parse(VisuNodeValues *vals, VisuNode *node, const gchar *from);
static gchar* _serialize(const VisuNodeValues *vals, const VisuNode *node);


G_DEFINE_TYPE(VisuNodeValuesType, visu_node_values_type, VISU_TYPE_NODE_VALUES)

static void visu_node_values_type_class_init(VisuNodeValuesTypeClass *klass)
{
  /* Connect the overloading methods. */
  VISU_NODE_VALUES_CLASS(klass)->getAt = _getAt;
  VISU_NODE_VALUES_CLASS(klass)->setAt = _setAt;
  VISU_NODE_VALUES_CLASS(klass)->parse = _parse;
  VISU_NODE_VALUES_CLASS(klass)->serialize = _serialize;
}

static void visu_node_values_type_init(VisuNodeValuesType *self _U_)
{
}

VisuElement* _getElement(const VisuNodeValues *vals, const VisuNode *node)
{
  VisuNodeArray *arr;
  VisuElement *ele;

  arr = visu_node_values_getArray(vals);
  ele = visu_node_array_getElement(arr, node);
  g_object_unref(arr);

  return ele;
}

static gboolean _parse(VisuNodeValues *vals, VisuNode *node, const gchar *from)
{
  gfloat fvals[3];
  gchar **datas;
  guint i;
  GValue value = {0, {{0}, {0}}};

  g_return_val_if_fail(from, FALSE);

  datas = g_strsplit_set(from, "(;)", 3);
  for (i = 0; datas[i]; i++)
    if (sscanf(from, "%f", fvals + i) != 1)
      {
        g_strfreev(datas);
        return FALSE;
      }
  g_strfreev(datas);
  g_value_init(&value, G_TYPE_POINTER);
  g_value_set_pointer(&value, fvals);
  return _setAt(vals, node, &value);
}
static gchar* _serialize(const VisuNodeValues *vals, const VisuNode *node)
{
  return g_strdup(visu_element_getName(_getElement(vals, node)));
}

/**
 * visu_node_values_type_new:
 * @array: a #VisuData object.
 *
 * Create a #VisuNodeValues object to handle typeinates of nodes.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a newly created #VisuNodeValuesType object.
 **/
VisuNodeValuesType* visu_node_values_type_new(VisuNodeArray *array)
{
  return g_object_new(VISU_TYPE_NODE_VALUES_TYPE,
                      "label", _("Type"),
                      "internal", TRUE,
                      "nodes", array,
                      "type", VISU_TYPE_ELEMENT,
                      "n-elements", 1,
                      "editable", FALSE,
                      NULL);
}

/**
 * visu_node_values_type_getAt:
 * @vect: a #VisuNodeValuesType object.
 * @node: a #VisuNode object.
 *
 * Retrieves the type hosted on @node.
 *
 * Since: 3.8
 *
 * Returns: (transfer none): the #VisuElement for @node.
 **/
VisuElement* visu_node_values_type_getAt(const VisuNodeValuesType *vect,
                                         const VisuNode *node)
{
  g_return_val_if_fail(VISU_IS_NODE_VALUES_TYPE(vect), (VisuElement*)0);

  return _getElement(VISU_NODE_VALUES(vect), node);
}

static gboolean _getAt(const VisuNodeValues *vals, const VisuNode *node,
                       GValue *value)
{
  g_value_set_object(value, _getElement(vals, node));
  return TRUE;
}
static gboolean _setAt(VisuNodeValues *vect, const VisuNode *node,
                       GValue *value)
{
  VisuNodeArray *arr;
  VisuNode *n, *newNode;

  arr = visu_node_values_getArray(vect);
  n = visu_node_array_getFromId(arr, node->number);
  newNode = visu_node_array_setElement(arr, n,
                                       VISU_ELEMENT(g_value_get_object(value)));
  g_object_unref(arr);

  /* Chain up to parent. */
  return VISU_NODE_VALUES_CLASS(visu_node_values_type_parent_class)->setAt(vect, newNode, value);
}
/**
 * visu_node_values_type_setAt:
 * @vect: a #VisuNodeValuesType object.
 * @node: a #VisuNode object.
 * @element: type typeinates.
 *
 * Changes the type hosted at @node for @element. This will change
 * @node and its location. @node is not valid anymore after a call to
 * this function.
 *
 * Since: 3.8
 *
 * Returns: TRUE if type for @node is indeed changed.
 **/
gboolean visu_node_values_type_setAt(VisuNodeValuesType *vect,
                                     const VisuNode *node,
                                     VisuElement *element)
{
  GValue value = G_VALUE_INIT;
  gboolean res;

  g_value_init(&value, VISU_TYPE_ELEMENT);
  g_value_set_object(&value, element);
  res = visu_node_values_setAt(VISU_NODE_VALUES(vect), node, &value);
  g_value_unset(&value);
  return res;
}
