/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2015-2019)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2015-2019)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef NODEPROP_H
#define NODEPROP_H

#include <glib.h>
#include <glib-object.h>

#include <visu_nodes.h>

G_BEGIN_DECLS

#define VISU_TYPE_NODE_VALUES	         (visu_node_values_get_type ())
#define VISU_NODE_VALUES(obj)	         (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_NODE_VALUES, VisuNodeValues))
#define VISU_NODE_VALUES_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_NODE_VALUES, VisuNodeValuesClass))
#define VISU_IS_NODE_VALUES(obj)         (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_NODE_VALUES))
#define VISU_IS_NODE_VALUES_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_NODE_VALUES))
#define VISU_NODE_VALUES_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_NODE_VALUES, VisuNodeValuesClass))

/**
 * VisuNodeValuesPrivate:
 * 
 * Private data for #VisuNodeValues objects.
 */
typedef struct _VisuNodeValuesPrivate VisuNodeValuesPrivate;

/**
 * VisuNodeValues:
 * 
 * Common name to refer to a #_VisuNodeValues.
 */
typedef struct _VisuNodeValues VisuNodeValues;
struct _VisuNodeValues
{
  VisuObject parent;

  VisuNodeValuesPrivate *priv;
};

/**
 * VisuNodeValuesFromString:
 * @vals: a #VisuNodeValues object.
 * @node: a #VisuNode pointer.
 * @string: a string.
 *
 * Prototype of functions used to parse @string and store its content
 * for @node in @vals.
 *
 * Since: 3.8
 *
 * Returns: TRUE on success.
 */
typedef gboolean (*VisuNodeValuesFromString)(VisuNodeValues *vals,
                                             VisuNode *node,
                                             const gchar *string);
/**
 * VisuNodeValuestoString:
 * @vals: a #VisuNodeValues object.
 * @node: a #VisuNode pointer.
 *
 * Prototype of functions used to stringify the node values of @node
 * from @vals.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a newly created string representing the
 * values of @node.
 */
typedef gchar* (*VisuNodeValuestoString)(const VisuNodeValues *vals,
                                         const VisuNode *node);
/**
 * VisuNodeValuesSetAt:
 * @vals: a #VisuNodeValues object.
 * @node: a #VisuNode pointer.
 * @value: a value.
 *
 * Prototype of functions used to store the content of @value for
 * @node in @vals.
 *
 * Since: 3.8
 *
 * Returns: TRUE on success.
 */
typedef gboolean (*VisuNodeValuesSetAt)(VisuNodeValues *vals,
                                        const VisuNode *node,
                                        GValue *value);
/**
 * VisuNodeValuesGetAt:
 * @vals: a #VisuNodeValues object.
 * @node: a #VisuNode pointer.
 * @value: a value.
 *
 * Prototype of functions used to store in @value the values
 * associated to @node from @vals.
 *
 * Since: 3.8
 *
 * Returns: TRUE on success.
 */
typedef gboolean (*VisuNodeValuesGetAt)(const VisuNodeValues *vals,
                                        const VisuNode *node,
                                        GValue *value);

/**
 * VisuNodeValuesClass:
 * @parent: private.
 * @parse: a routine to get values from a string.
 * @serialize: a routine to write format into a string.
 * @setAt: a routine to store values at a node.
 * @getAt: a routine to get values at a node.
 * 
 * Common name to refer to a #_VisuNodeValuesClass.
 */
typedef struct _VisuNodeValuesClass VisuNodeValuesClass;
struct _VisuNodeValuesClass
{
  VisuObjectClass parent;

  VisuNodeValuesFromString parse;
  VisuNodeValuestoString   serialize;

  VisuNodeValuesSetAt setAt;
  VisuNodeValuesGetAt getAt;
};

/**
 * visu_node_values_get_type:
 *
 * This method returns the type of #VisuNodeValues, use
 * VISU_TYPE_NODE_VALUES instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuNodeValues.
 */
GType visu_node_values_get_type(void);

VisuNodeValues* visu_node_values_new(VisuNodeArray *arr,
                                     const gchar *label,
                                     GType type, guint dim);
gboolean visu_node_values_fromArray(VisuNodeValues *vals, const VisuNodeArray *arr);

const gchar* visu_node_values_getLabel(const VisuNodeValues *vals);
guint visu_node_values_getDimension(const VisuNodeValues *vals);

void visu_node_values_setEditable(VisuNodeValues *vals, gboolean status);
gboolean visu_node_values_getEditable(const VisuNodeValues *vals);

gboolean visu_node_values_setFromString(VisuNodeValues *vals,
                                        VisuNode *node,
                                        const gchar *from);
gboolean visu_node_values_setFromStringForId(VisuNodeValues *vals,
                                             guint nodeId,
                                             const gchar *from);
gchar* visu_node_values_toString(const VisuNodeValues *vals,
                                 const VisuNode *node);
gchar* visu_node_values_toStringFromId(const VisuNodeValues *vals,
                                       guint nodeId);

gboolean visu_node_values_setAt(VisuNodeValues *vals, const VisuNode *node,
                                GValue *value);
gboolean visu_node_values_getAt(const VisuNodeValues *vals,
                                const VisuNode *node,
                                GValue *value);
gpointer visu_node_values_getPtrAt(const VisuNodeValues *vals,
                                   const VisuNode *node);
gboolean visu_node_values_copy(VisuNodeValues *vals, const VisuNodeValues *from);

VisuNodeArray* visu_node_values_getArray(const VisuNodeValues *vals);

void visu_node_values_reset(VisuNodeValues *vals);

/**
 * VisuNodeValuesIter:
 * @value: the stored value.
 * @iter: the iterator used to span nodes.
 * @vals: the parent #VisuNodeValues object.
 *
 * Iterator structure used to read values of nodes.
 *
 * Since: 3.8
 */
typedef struct _VisuNodeValuesIter VisuNodeValuesIter;
struct _VisuNodeValuesIter
{
  GValue value;
  VisuNodeArrayIter iter;
  
  /* Private. */
  VisuNodeValues *vals;
};
gboolean visu_node_values_iter_new(VisuNodeValuesIter *iter,
                                   VisuNodeArrayIterType type,
                                   VisuNodeValues *vals);
void visu_node_values___iter__(VisuNodeValues *vals, VisuNodeValuesIter *iter);
gboolean visu_node_values_iter_next(VisuNodeValuesIter *iter);

G_END_DECLS

#endif
