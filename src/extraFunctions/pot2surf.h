/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef POT2SURF_H
#define POT2SURF_H

#include "surfaces.h"
#include "scalarFields.h"

#ifndef V_SIM_DISABLE_DEPRECATED
int visu_surface_createFromPotentialFile(const gchar *surf_file_to_write,
                                          const gchar *pot_file_to_read,
                                          int nsurfs_to_build, 
                                          const float *surf_value,
                                          const gchar **surf_name);
#endif

VisuSurface* visu_surface_new_fromScalarField(const VisuScalarField *field,
                                              double isoValue, const gchar *name);
void visu_surface_new_defaultFromScalarField(const VisuScalarField *field,
                                             VisuSurface **neg,
                                             VisuSurface **pos);

gboolean visu_surface_parseXMLFile(const gchar* filename, GList **surfaces,
			       VisuScalarField *field, GError **error);
gboolean visu_surface_exportXMLFile(const gchar* filename, float *values,
                                     VisuSurfaceResource **res, int n, GError **error);

#endif
