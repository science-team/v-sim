/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2014)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2014)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef IFACE_LINED_H
#define IFACE_LINED_H

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

/* GlExtLined interface. */
#define VISU_TYPE_GL_EXT_LINED                (visu_gl_ext_lined_get_type ())
#define VISU_GL_EXT_LINED(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_TYPE_GL_EXT_LINED, VisuGlExtLined))
#define VISU_IS_GL_EXT_LINED(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_TYPE_GL_EXT_LINED))
#define VISU_GL_EXT_LINED_GET_INTERFACE(inst) (G_TYPE_INSTANCE_GET_INTERFACE ((inst), VISU_TYPE_GL_EXT_LINED, VisuGlExtLinedInterface))

typedef struct _VisuGlExtLined VisuGlExtLined; /* dummy object */
typedef struct _VisuGlExtLinedInterface VisuGlExtLinedInterface;

/**
 * VisuGlExtLined:
 *
 * Interface object.
 *
 * Since: 3.8
 */

/**
 * VisuGlExtLinedInterface:
 * @parent: yet, its parent.
 * @get_width: a routine to get line width.
 * @set_width: a routine to set line width.
 * @get_stipple: a routine to get line stipple.
 * @set_stipple: a routine to set line stipple.
 * @get_rgba: a routine to get line color.
 * @set_rgba: a routine to set line color.
 *
 * The different routines common to objects implementing a #VisuGlExtLined interface.
 * 
 * Since: 3.8
 */
struct _VisuGlExtLinedInterface
{
  GTypeInterface parent;

  gfloat  (*get_width)   (const VisuGlExtLined *self);
  guint16 (*get_stipple) (const VisuGlExtLined *self);
  gfloat* (*get_rgba)    (const VisuGlExtLined *self);

  gboolean (*set_width)   (VisuGlExtLined *self, gfloat value);
  gboolean (*set_stipple) (VisuGlExtLined *self, guint16 value);
  gboolean (*set_rgba)    (VisuGlExtLined *self, gfloat values[4], gint mask);
};

GType visu_gl_ext_lined_get_type (void);

gfloat  visu_gl_ext_lined_getWidth   (const VisuGlExtLined *self);
guint16 visu_gl_ext_lined_getStipple (const VisuGlExtLined *self);
gfloat* visu_gl_ext_lined_getRGBA    (const VisuGlExtLined *self);

gboolean visu_gl_ext_lined_setWidth   (VisuGlExtLined *self, gfloat value);
gboolean visu_gl_ext_lined_setStipple (VisuGlExtLined *self, guint16 value);
gboolean visu_gl_ext_lined_setRGBA    (VisuGlExtLined *self, gfloat values[4], gint mask);

G_END_DECLS

#endif
