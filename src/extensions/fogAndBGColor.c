/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "fogAndBGColor.h"

#include <string.h>

#include <opengl.h>
#include <visu_tools.h>
#include <visu_configFile.h>
#include <visu_extset.h>
#include <openGLFunctions/objectList.h>
#include <openGLFunctions/text.h>
#include <openGLFunctions/view.h>
#include <coreTools/toolColor.h>

/* For the GdkPixbuf, to be removed later. */
#include <gtk/gtk.h>

#include <GL/gl.h>
#include <GL/glu.h>

/**
 * SECTION:fogAndBGColor
 * @short_description: Handle the background colour and the fog.
 *
 * This module is used to support a background colour and to tune the
 * fog. This last one can be turn on or off and its colour can be
 * either a user defined one or the one of the background. The fog is
 * a linear blending into the fog colour. It starts at a given z
 * position (in the camera basis set) and ends at a lower z.
 */

/**
 * VisuGlExtBgClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtBgClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtBg:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtBgPrivate:
 *
 * Private fields for #VisuGlExtBg objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtBgPrivate
{
  gboolean dispose_has_run;

  /* Handling the background image. */
  guchar *bgImage;
  gboolean bgImageAlpha, bgImageFit, bgImageFollowZoom;
  guint bgImageW, bgImageH;
  gchar *bgImageTitle;
  float bgImageZoomInit, bgImageZoom, bgImageZoomRatioInit;
  float bgImageXsInit, bgImageXs, bgImageXs0;
  float bgImageYsInit, bgImageYs, bgImageYs0;

  gchar *bgFile;
  gboolean withLabel;

  /* Signals for the current view. */
  VisuGlView *view;
  gulong widthHeight_signal;
  gulong transx_signal, transy_signal, gross_signal;
};

enum
  {
    PROP_0,
    BG_IMAGE_PROP,
    BG_LABEL_PROP,
    N_PROP,
  };
static GParamSpec *_properties[N_PROP];

static void visu_gl_ext_bg_finalize(GObject* obj);
static void visu_gl_ext_bg_dispose(GObject* obj);
static void visu_gl_ext_bg_get_property(GObject* obj, guint property_id,
                                        GValue *value, GParamSpec *pspec);
static void visu_gl_ext_bg_set_property(GObject* obj, guint property_id,
                                        const GValue *value, GParamSpec *pspec);
static void visu_gl_ext_bg_rebuild(VisuGlExt *ext);
static void visu_gl_ext_bg_draw(VisuGlExt *bg);
static gboolean visu_gl_ext_bg_setGlView(VisuGlExt *bg, VisuGlView *view);

/* Local callbacks */
static void onBgImageRescale(VisuGlView *view, gpointer data);
static void onCameraChange(VisuGlView *view, GParamSpec *pspec, gpointer data);

G_DEFINE_TYPE_WITH_CODE(VisuGlExtBg, visu_gl_ext_bg, VISU_TYPE_GL_EXT,
                        G_ADD_PRIVATE(VisuGlExtBg))

static void visu_gl_ext_bg_class_init(VisuGlExtBgClass *klass)
{
  DBG_fprintf(stderr, "Extension Bg: creating the class of the object.\n");
  /* DBG_fprintf(stderr, "                - adding new signals ;\n"); */

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_bg_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_bg_finalize;
  G_OBJECT_CLASS(klass)->set_property = visu_gl_ext_bg_set_property;
  G_OBJECT_CLASS(klass)->get_property = visu_gl_ext_bg_get_property;
  VISU_GL_EXT_CLASS(klass)->rebuild = visu_gl_ext_bg_rebuild;
  VISU_GL_EXT_CLASS(klass)->draw = visu_gl_ext_bg_draw;
  VISU_GL_EXT_CLASS(klass)->setGlView = visu_gl_ext_bg_setGlView;

  /**
   * VisuGlNodeScene::background-file:
   *
   * Path to the background image.
   *
   * Since: 3.8
   */
  _properties[BG_IMAGE_PROP] =
    g_param_spec_string("background-file", "Background file",
                        "path to the background image.",
                        "", G_PARAM_READWRITE);
  /**
   * VisuGlNodeScene::display-background-filename:
   *
   * Display or not the background filename.
   *
   * Since: 3.8
   */
  _properties[BG_LABEL_PROP] =
    g_param_spec_boolean("display-background-filename", "Display background filename",
                         "display or not the background filename.",
                         FALSE, G_PARAM_READWRITE);

  g_object_class_install_properties(G_OBJECT_CLASS(klass), N_PROP, _properties);
}

static void visu_gl_ext_bg_init(VisuGlExtBg *obj)
{
  DBG_fprintf(stderr, "Extension Bg: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = visu_gl_ext_bg_get_instance_private(obj);
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->bgImage      = (guchar*)0;
  obj->priv->bgImageTitle = (gchar*)0;
  obj->priv->bgImageFollowZoom = FALSE;
  obj->priv->bgImageZoomInit = obj->priv->bgImageZoom = -1.f;
  obj->priv->bgImageZoomRatioInit = 1.f;
  obj->priv->bgImageXsInit = obj->priv->bgImageXs = 0.5f;
  obj->priv->bgImageXs0 = 0.f;
  obj->priv->bgImageYsInit = obj->priv->bgImageYs = 0.5f;
  obj->priv->bgImageYs0 = 0.f;
  obj->priv->view               = (VisuGlView*)0;
  obj->priv->widthHeight_signal = 0;
  obj->priv->bgFile = (gchar*)0;
  obj->priv->withLabel = FALSE;
}
static void visu_gl_ext_bg_dispose(GObject* obj)
{
  VisuGlExtBg *bg;

  DBG_fprintf(stderr, "Extension Bg: dispose object %p.\n", (gpointer)obj);

  bg = VISU_GL_EXT_BG(obj);
  if (bg->priv->dispose_has_run)
    return;
  bg->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  visu_gl_ext_bg_setGlView(VISU_GL_EXT(bg), (VisuGlView*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_bg_parent_class)->dispose(obj);
}
static void visu_gl_ext_bg_finalize(GObject* obj)
{
  VisuGlExtBg *bg;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Extension Bg: finalize object %p.\n", (gpointer)obj);

  bg = VISU_GL_EXT_BG(obj);

  /* Free privs elements. */
  DBG_fprintf(stderr, "Extension Bg: free private bg.\n");
  g_free(bg->priv->bgImage);
  g_free(bg->priv->bgImageTitle);
  g_free(bg->priv->bgFile);

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Extension Bg: chain to parent.\n");
  G_OBJECT_CLASS(visu_gl_ext_bg_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Extension Bg: freeing ... OK.\n");
}
static void visu_gl_ext_bg_get_property(GObject* obj, guint property_id,
                                        GValue *value, GParamSpec *pspec)
{
  VisuGlExtBg *self = VISU_GL_EXT_BG(obj);

  DBG_fprintf(stderr, "Extension Bg: get property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case BG_IMAGE_PROP:
      g_value_set_string(value, self->priv->bgFile);
      DBG_fprintf(stderr, "%s.\n", g_value_get_string(value));
      break;
    case BG_LABEL_PROP:
      g_value_set_boolean(value, self->priv->withLabel);
      DBG_fprintf(stderr, "%d.\n", g_value_get_boolean(value));
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_gl_ext_bg_set_property(GObject* obj, guint property_id,
                                        const GValue *value, GParamSpec *pspec)
{
  VisuGlExtBg *self = VISU_GL_EXT_BG(obj);
  GError *error;
  gchar *path;

  DBG_fprintf(stderr, "Extension Bg: set property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case BG_IMAGE_PROP:
      DBG_fprintf(stderr, "%s.\n", g_value_get_string(value));
      error = (GError*)0;
      visu_gl_ext_bg_setFile(self, g_value_get_string(value), &error);
      if (error)
        {
          g_warning("%s", error->message);
          g_error_free(error);
        }
      break;
    case BG_LABEL_PROP:
      DBG_fprintf(stderr, "%d.\n", g_value_get_boolean(value));
      self->priv->withLabel = g_value_get_boolean(value);
      path = self->priv->bgFile;
      self->priv->bgFile = (gchar*)0;
      error = (GError*)0;
      visu_gl_ext_bg_setFile(self, path, &error);
      if (error)
        {
          g_warning("%s", error->message);
          g_error_free(error);
        }
      g_free(path);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
/**
 * visu_gl_ext_bg_new:
 * @name: (allow-none): the name to give to the extension (default is #VISU_GL_EXT_BG_ID).
 *
 * Creates a new #VisuGlExt to draw bg.
 *
 * Since: 3.7
 *
 * Returns: a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtBg* visu_gl_ext_bg_new(const gchar *name)
{
  char *name_ = VISU_GL_EXT_BG_ID;
  char *description = _("Set an image as background.");
  VisuGlExt *extensionBg;

  DBG_fprintf(stderr,"Extension Bg: new object.\n");
  
  extensionBg = VISU_GL_EXT(g_object_new(VISU_TYPE_GL_EXT_BG,
                                              "name", (name)?name:name_, "label", _(name),
                                              "description", description, "nGlObj", 1,
                                              "priority", VISU_GL_EXT_PRIORITY_BACKGROUND,
                                              "saveState", TRUE, NULL));

  return VISU_GL_EXT_BG(extensionBg);
}

static gboolean visu_gl_ext_bg_setGlView(VisuGlExt *bg, VisuGlView *view)
{
  VisuGlExtBgPrivate *priv = ((VisuGlExtBg*)bg)->priv;

  /* No change to be done. */
  if (view == priv->view)
    return FALSE;

  if (priv->view)
    {
      g_signal_handler_disconnect(G_OBJECT(priv->view), priv->widthHeight_signal);
      g_signal_handler_disconnect(G_OBJECT(priv->view), priv->transx_signal);
      g_signal_handler_disconnect(G_OBJECT(priv->view), priv->transy_signal);
      g_signal_handler_disconnect(G_OBJECT(priv->view), priv->gross_signal);
      g_object_unref(priv->view);
    }
  if (view)
    {
      g_object_ref(view);
      priv->widthHeight_signal =
        g_signal_connect(G_OBJECT(view), "WidthHeightChanged",
                         G_CALLBACK(onBgImageRescale), (gpointer)bg);
      priv->transx_signal =
        g_signal_connect(G_OBJECT(view), "notify::trans-x",
                         G_CALLBACK(onCameraChange), (gpointer)bg);
      priv->transy_signal =
        g_signal_connect(G_OBJECT(view), "notify::trans-y",
                         G_CALLBACK(onCameraChange), (gpointer)bg);
      priv->gross_signal =
        g_signal_connect(G_OBJECT(view), "notify::zoom",
                         G_CALLBACK(onCameraChange), (gpointer)bg);
    }
  else
    {
      priv->widthHeight_signal = 0;
    }
  priv->view = view;

  visu_gl_ext_setDirty(bg, TRUE);
  return TRUE;
}

/**
 * visu_gl_ext_bg_setFile:
 * @bg: a #VisuGlExtBg object.
 * @path: a file path
 * @error: an error location.
 *
 * Loads @path and store it as a background image for the scene, see
 * visu_gl_ext_bg_setImage().
 *
 * Since: 3.8
 *
 * Returns: FALSE if an error occured.
 **/
gboolean visu_gl_ext_bg_setFile(VisuGlExtBg *bg, const gchar *path,
                                GError **error)
{
  GdkPixbuf *pixbuf;
  gchar *title;
  gboolean fit;

  g_return_val_if_fail(VISU_IS_GL_EXT_BG(bg), FALSE);

  if (!g_strcmp0(bg->priv->bgFile, path))
    return FALSE;

  g_free(bg->priv->bgFile);
  bg->priv->bgFile = (gchar*)0;

  if (!path)
    {
      visu_gl_ext_bg_setImage(bg, (guchar*)0, 0, 0, FALSE, (const gchar*)0, TRUE);
      g_object_notify_by_pspec(G_OBJECT(bg), _properties[BG_IMAGE_PROP]);
      return TRUE;
    }
  
  pixbuf = gdk_pixbuf_new_from_file(path, error);
  if (!pixbuf)
    {
      visu_gl_ext_bg_setImage(bg, (guchar*)0, 0, 0, FALSE, (const gchar*)0, TRUE);
      g_object_notify_by_pspec(G_OBJECT(bg), _properties[BG_IMAGE_PROP]);
      return TRUE;
    }

  fit = TRUE;
  title = g_path_get_basename(path);
  if (!strcmp(title, "logo_grey.png"))
    {
      fit = FALSE;
      g_free(title);
      title = (gchar*)0;
    }
  visu_gl_ext_bg_setImage(bg,
                          gdk_pixbuf_get_pixels(pixbuf),
                          gdk_pixbuf_get_width(pixbuf),
                          gdk_pixbuf_get_height(pixbuf),
                          gdk_pixbuf_get_has_alpha(pixbuf),
                          bg->priv->withLabel ? title : (const gchar*)0, fit);
  g_object_unref(pixbuf);
  g_free(title);
  bg->priv->bgFile = g_strdup(path);
  g_object_notify_by_pspec(G_OBJECT(bg), _properties[BG_IMAGE_PROP]);
  return TRUE;
}
/**
 * visu_gl_ext_bg_setImage:
 * @bg: a #VisuGlExtBg object.
 * @imageData: (allow-none): raw image data in RGB or RGBA format ;
 * @width: the width ;
 * @height: the height ;
 * @alpha: TRUE if the image is RGBA ;
 * @title: (allow-none): an optional title (can be NULL).
 * @fit: a boolean (default is TRUE).
 *
 * Draw the @imageData on the background. The image is scaled to the
 * viewport dimensions, keeping the width/height ratio, if @fit is set
 * to TRUE. If @title is not NULL, the title is also printed on the
 * background. The image data are copied and can be free after this
 * call.
 */
void visu_gl_ext_bg_setImage(VisuGlExtBg *bg,
                             const guchar *imageData, guint width, guint height,
                             gboolean alpha, const gchar *title, gboolean fit)
{
  guint n;

  g_return_if_fail(VISU_IS_GL_EXT_BG(bg));

  g_free(bg->priv->bgImage);
  bg->priv->bgImage = (guchar*)0;
  g_free(bg->priv->bgImageTitle);
  bg->priv->bgImageTitle = (gchar*)0;

  visu_gl_ext_setDirty(VISU_GL_EXT(bg), TRUE);

  if (!imageData)
    return;

  DBG_fprintf(stderr, "Extension bg: copy image to memory buffer.\n");
  /* We copy the image to some correct size buffer. */
  bg->priv->bgImageW = width;
  bg->priv->bgImageH = height;
  n = (alpha)?4:3;
  bg->priv->bgImage = g_memdup(imageData, sizeof(guchar) * bg->priv->bgImageW *
                               bg->priv->bgImageH * n);
  bg->priv->bgImageAlpha = alpha;
  if (title)
    bg->priv->bgImageTitle = g_strdup(title);
  bg->priv->bgImageFit = fit;
  bg->priv->bgImageZoomInit = bg->priv->bgImageZoom = -1.f;
  bg->priv->bgImageZoomRatioInit = 1.f;
  bg->priv->bgImageXsInit = bg->priv->bgImageXs = 0.5f;
  bg->priv->bgImageXs0 = 0.f;
  bg->priv->bgImageYsInit = bg->priv->bgImageYs = 0.5f;
  bg->priv->bgImageYs0 = 0.f;
}
/**
 * visu_gl_ext_bg_setFollowCamera:
 * @bg: a #VisuGlExtBg object.
 * @follow: a boolean.
 * @zoomInit: a floating point value.
 * @xs: a floating point value.
 * @ys: a floating point value.
 *
 * When @follow is TRUE, the size and the position of the background
 * image is adjusted with every camera change.
 *
 * Since: 3.7
 *
 * Returns: TRUE if the following status has been changed.
 */
gboolean visu_gl_ext_bg_setFollowCamera(VisuGlExtBg *bg, gboolean follow, float zoomInit,
                                        float xs, float ys)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BG(bg), FALSE);

  if (follow == bg->priv->bgImageFollowZoom)
    return FALSE;

  bg->priv->bgImageFollowZoom = follow;
  if (follow)
    {
      bg->priv->bgImageZoomInit = bg->priv->bgImageZoom = zoomInit;
      bg->priv->bgImageXsInit = bg->priv->bgImageXs = xs;
      bg->priv->bgImageYsInit = bg->priv->bgImageYs = ys;
    }
  else
    {
      bg->priv->bgImageZoomRatioInit *= bg->priv->bgImageZoom / bg->priv->bgImageZoomInit;
      bg->priv->bgImageXs0 -= bg->priv->bgImageXs - bg->priv->bgImageXsInit;
      bg->priv->bgImageYs0 -= bg->priv->bgImageYs - bg->priv->bgImageYsInit;
    }

  visu_gl_ext_setDirty(VISU_GL_EXT(bg), TRUE);
  return TRUE;
}
/**
 * visu_gl_ext_bg_setCamera:
 * @bg: a #VisuGlExtBg object.
 * @zoom: a floating point value.
 * @xs: a floating point value.
 * @ys: a floating point value.
 *
 * If the background image is in follow mode, see
 * visu_gl_ext_bg_setFollowCamera(), this routine is used to update
 * the current camera settings of the background image.
 *
 * Since: 3.7
 *
 * Returns: TRUE if the settings are indeed changed.
 */
gboolean visu_gl_ext_bg_setCamera(VisuGlExtBg *bg, float zoom, float xs, float ys)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BG(bg), FALSE);

  if (zoom == bg->priv->bgImageZoom && xs == bg->priv->bgImageXs && ys == bg->priv->bgImageYs)
    return FALSE;

  if (bg->priv->bgImageFollowZoom)
    {
      bg->priv->bgImageZoom = zoom;
      bg->priv->bgImageXs = xs;
      bg->priv->bgImageYs = ys;
    }

  return bg->priv->bgImageFollowZoom;
}

/****************/
/* Private part */
/****************/
static void visu_gl_ext_bg_draw(VisuGlExt *ext)
{
  VisuGlExtBg *bg = VISU_GL_EXT_BG(ext);
  int viewport[4];
  float x, y;
  float zoom;

  DBG_fprintf(stderr, "Extension BG: redrawing.\n");

  glDeleteLists(visu_gl_ext_getGlList(ext), 1);
  visu_gl_ext_setDirty(ext, FALSE);
  
  /* Nothing to draw; */
  if(!bg->priv->view || !bg->priv->bgImage) return;

  DBG_fprintf(stderr, "Extension bg: set background image.\n");

  visu_gl_text_initFontList();
  
  glGetIntegerv(GL_VIEWPORT, viewport);
  if (bg->priv->bgImageFit)
    {
      x = (float)viewport[2] / (float)bg->priv->bgImageW;
      y = (float)viewport[3] / (float)bg->priv->bgImageH;
    }
  else
    {
      x = y = 1.f;
    }
  DBG_fprintf(stderr, "Extension bg: use follow zoom %d (%g / %g).\n",
              bg->priv->bgImageFollowZoom, bg->priv->bgImageZoom, bg->priv->bgImageZoomInit);
  zoom = MIN(x, y) * bg->priv->bgImageZoomRatioInit * bg->priv->bgImageZoom / bg->priv->bgImageZoomInit;
  x = ((float)viewport[2] - zoom * (float)bg->priv->bgImageW) / 2.f;
  x += (float)viewport[2] * (bg->priv->bgImageXs - bg->priv->bgImageXsInit - bg->priv->bgImageXs0);
  y = ((float)viewport[3] - zoom * (float)bg->priv->bgImageH) / 2.f;
  y -= (float)viewport[3] * (bg->priv->bgImageYs - bg->priv->bgImageYsInit - bg->priv->bgImageYs0);

  visu_gl_ext_startDrawing(ext);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  gluOrtho2D(0., (float)viewport[2], 0., (float)viewport[3]);   
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  glDepthMask(0);
  glRasterPos2i(0, 0);
  glBitmap(0, 0, 0, 0, x, viewport[3] - y, NULL);
  glPixelZoom(zoom, -zoom);
  if (bg->priv->bgImageAlpha)
    glDrawPixels(bg->priv->bgImageW, bg->priv->bgImageH, GL_RGBA, GL_UNSIGNED_BYTE, bg->priv->bgImage);
  else
    glDrawPixels(bg->priv->bgImageW, bg->priv->bgImageH, GL_RGB, GL_UNSIGNED_BYTE, bg->priv->bgImage);
  glPixelZoom(1., 1.);

  if (bg->priv->bgImageTitle)
    {
      glDisable(GL_LIGHTING);
      glColor4f(0.5f, 0.5f, 0.5f, 1.f);
      glRasterPos2f(5.f, 5.f);
      visu_gl_text_drawChars(bg->priv->bgImageTitle, VISU_GL_TEXT_NORMAL); 
    }
  glDepthMask(1);

  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);

  visu_gl_ext_completeDrawing(ext);
}

static void visu_gl_ext_bg_rebuild(VisuGlExt *ext)
{
  DBG_fprintf(stderr, "Fog & Bg: rebuild extension.\n");
  visu_gl_ext_setDirty(ext, TRUE);
  visu_gl_ext_bg_draw(ext);
}
static void onBgImageRescale(VisuGlView *view _U_, gpointer data)
{
  DBG_fprintf(stderr, "Extension Bg: caught the 'WidthHeightChanged' signal.\n");

  if (VISU_GL_EXT_BG(data)->priv->bgImage)
    visu_gl_ext_setDirty(VISU_GL_EXT(data), TRUE);
}
static void onCameraChange(VisuGlView *view, GParamSpec *pspec _U_, gpointer data)
{
  if (visu_gl_ext_bg_setCamera(VISU_GL_EXT_BG(data),
                               view->camera.gross, view->camera.xs, view->camera.ys))
    visu_gl_ext_setDirty(VISU_GL_EXT(data), TRUE);
}
