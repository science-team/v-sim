/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef MARKS_H
#define MARKS_H

#include "infos.h"
#include <iface_nodemasker.h>
#include <renderingMethods/iface_nodeArrayRenderer.h>
#include <openGLFunctions/interactive.h>

/**
 * VisuGlExtMarksStatus:
 * @MARKS_STATUS_KEEP: do not change the status of the mark ;
 * @MARKS_STATUS_TOGGLE: change the status of the mark ;
 * @MARKS_STATUS_SET: set the mark on ;
 * @MARKS_STATUS_UNSET: unset a mark.
 *
 * Possible parameters to change mark status, see
 * visu_gl_ext_marks_setHighlightedList() for instance.
 *
 * Since: 3.6
 */
typedef enum
  {
    MARKS_STATUS_KEEP,
    MARKS_STATUS_TOGGLE,
    MARKS_STATUS_SET,
    MARKS_STATUS_UNSET
  } VisuGlExtMarksStatus;
/**
 * VisuGlExtMarksHidingModes:
 * @HIDE_NONE: don't hide nodes.
 * @HIDE_HIGHLIGHT: hide highlighted nodes.
 * @HIDE_NON_HIGHLIGHT: hide all non-highlighted nodes.
 *
 * Possible parameters used by visu_gl_ext_marks_setHidingMode().
 *
 * Since: 3.8
 */
typedef enum
  {
    HIDE_NONE,
    HIDE_HIGHLIGHT,
    HIDE_NON_HIGHLIGHT
  } VisuGlExtMarksHidingModes;

/**
 * VISU_TYPE_GL_EXT_MARKS:
 *
 * Return the associated #GType to the #VisuGlExtMarks objects.
 *
 * Since: 3.6
 */
#define VISU_TYPE_GL_EXT_MARKS          (visu_gl_ext_marks_get_type())
/**
 * VISU_GL_EXT_MARKS:
 * @obj: the widget to cast.
 *
 * Cast the given object to a #VisuGlExtMarks object.
 *
 * Since: 3.6
 */
#define VISU_GL_EXT_MARKS(obj)          (G_TYPE_CHECK_INSTANCE_CAST((obj), VISU_TYPE_GL_EXT_MARKS, VisuGlExtMarks))
/**
 * VISU_GL_EXT_MARKS_CLASS:
 * @obj: the class to cast.
 *
 * Cast the given class to a #VisuGlExtMarks object.
 *
 * Since: 3.6
 */
#define VISU_GL_EXT_MARKS_CLASS(obj)    (G_TYPE_CHECK_CLASS_CAST((obj), VISU_GL_EXT_MARKS, VisuGlExtMarksClass))
/**
 * VISU_IS_GL_EXT_MARKS:
 * @obj: the object to test.
 *
 * Return if the given object is a valid #VisuGlExtMarks object.
 *
 * Since: 3.6
 */
#define VISU_IS_GL_EXT_MARKS(obj)       (G_TYPE_CHECK_INSTANCE_TYPE((obj), VISU_TYPE_GL_EXT_MARKS))
/**
 * VISU_IS_GL_EXT_MARKS_CLASS:
 * @obj: the class to test.
 *
 * Return if the given class is a valid #VisuGlExtMarksClass class.
 *
 * Since: 3.6
 */
#define VISU_IS_GL_EXT_MARKS_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE((obj), VISU_TYPE_GL_EXT_MARKS))
/**
 * VISU_GL_EXT_MARKS_GET_CLASS:
 * @obj: the widget to get the class of.
 *
 * Get the class of the given object.
 *
 * Since: 3.6
 */
#define VISU_GL_EXT_MARKS_GET_CLASS(obj)     (G_TYPE_INSTANCE_GET_CLASS((obj), VISU_TYPE_GL_EXT_MARKS, VisuGlExtMarksClass))

typedef struct _VisuGlExtMarks            VisuGlExtMarks;
typedef struct _VisuGlExtMarksClass       VisuGlExtMarksClass;
typedef struct _VisuGlExtMarksPrivate     VisuGlExtMarksPrivate;

/**
 * VisuGlExtMarks:
 *
 * All fields are private.
 *
 * Since: 3.6
 */
struct _VisuGlExtMarks
{
  VisuGlExt parent;

  VisuGlExtMarksPrivate *priv;
};

/**
 * VisuGlExtMarksClass:
 * @parent: the parent class.
 *
 * An opaque structure defining the class of a #VisuGlExtMarks objects.
 *
 * Since: 3.6
 */
struct _VisuGlExtMarksClass
{
  VisuGlExtClass parent;
};

/**
 * visu_gl_ext_marks_get_type:
 *
 * Internal routine to get #VISU_TYPE_GL_EXT_MARKS value.
 *
 * Since: 3.6
 */
GType visu_gl_ext_marks_get_type(void);

VisuGlExtMarks* visu_gl_ext_marks_new(const gchar *name);

void visu_gl_ext_marks_setDataRenderer(VisuGlExtMarks *marks, VisuNodeArrayRenderer *renderer);
void visu_gl_ext_marks_setInteractive(VisuGlExtMarks *marks, VisuInteractive *inter);

gboolean visu_gl_ext_marks_setHighlight(VisuGlExtMarks *marks, GArray *nodes,
                                        VisuGlExtMarksStatus status);
gboolean visu_gl_ext_marks_unHighlight(VisuGlExtMarks *marks);
GArray* visu_gl_ext_marks_getHighlighted(const VisuGlExtMarks *marks);
gboolean visu_gl_ext_marks_setInfos(VisuGlExtMarks *marks, guint nodeId, gboolean status);
gboolean visu_gl_ext_marks_getActive(VisuGlExtMarks *marks, guint nodeId);
gboolean visu_gl_ext_marks_getHighlightStatus(VisuGlExtMarks *marks, guint nodeId);
gboolean visu_gl_ext_marks_setDrawValues(VisuGlExtMarks *marks, gboolean status);
gboolean visu_gl_ext_marks_removeMeasures(VisuGlExtMarks *marks, gint nodeId);

gboolean visu_gl_ext_marks_parseXMLFile(VisuGlExtMarks *marks, const gchar* filename,
				GList **infos, VisuGlExtInfosDrawId *drawingMode,
				guint *drawingInfos, GError **error);
gboolean visu_gl_ext_marks_exportXMLFile(VisuGlExtMarks *marks, const gchar* filename,
                                         GArray *nodes, VisuGlExtInfosDrawId drawingMode,
                                         guint drawingInfos, GError **error);
gchar* visu_gl_ext_marks_getMeasurementStrings(VisuGlExtMarks *marks);
gchar* visu_gl_ext_marks_getMeasurementLabels(VisuGlExtMarks *marks);
VisuGlExt* visu_gl_ext_marks_getInternalList(VisuGlExtMarks *marks);

VisuGlExtMarksHidingModes visu_gl_ext_marks_getHidingMode(const VisuGlExtMarks *marks);
gboolean visu_gl_ext_marks_setHidingMode(VisuGlExtMarks *marks,
                                         VisuGlExtMarksHidingModes mode);

#endif
