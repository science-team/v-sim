/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef GTK_INTERACTIVE_H
#define GTK_INTERACTIVE_H

#include <gtk/gtk.h>

#include "gtk_main.h"
#include "gtk_renderingWindowWidget.h"

/**
 * VisuUiInteractiveActionId:
 * @VISU_UI_ACTION_OBSERVE: interactive session is observe ;
 * @VISU_UI_ACTION_PICK: interactive session is pick ;
 * @VISU_UI_ACTION_MOVE: interactive session is geometry changes.
 * @VISU_UI_ACTION_N_PRESET: private.
 *
 * Possibe actions.
 */
typedef enum
  {
    VISU_UI_ACTION_OBSERVE,
    VISU_UI_ACTION_PICK,
    VISU_UI_ACTION_MOVE,
    /*< private >*/
    VISU_UI_ACTION_N_PRESET
  } VisuUiInteractiveActionId;

/**
 * VisuUiInteractiveBuild:
 * @main: the main interface.
 * @label: a location to store the name of the tab ;
 * @help: a location to store the help message to be shown at the
 * bottom of the window ;
 * @radio: a location on the radio button that will be toggled when
 * the desired action is used.
 *
 * One can create new tab in the interactive dialog window by
 * providing routines with this prototype.
 *
 * Returns: a new container to be include as a tab in the interactive
 * dialog window.
 */
typedef GtkWidget* (*VisuUiInteractiveBuild)(VisuUiMain *main, gchar **label,
                                             gchar **help, GtkWidget **radio); 
/**
 * VisuUiInteractiveStartStop:
 * @window: the rendering window that starts or stops the interaction
 * defined in the tab.
 *
 * Routines of this prototype are called each time the interactive
 * mode should be changed.
 */
typedef void (*VisuUiInteractiveStartStop)(VisuUiRenderingWindow *window); 

guint visu_ui_interactive_addAction(VisuUiInteractiveBuild build,
			       VisuUiInteractiveStartStop start,
			       VisuUiInteractiveStartStop stop);

void visu_ui_interactive_init();
void visu_ui_interactive_toggle(void);

void visu_ui_interactive_initBuild(VisuUiMain *main);

void visu_ui_interactive_start(VisuUiRenderingWindow *window);
void visu_ui_interactive_stop(VisuUiRenderingWindow *window);

void visu_ui_interactive_setMessage(const gchar *message, GtkMessageType type);
void visu_ui_interactive_unsetMessage();

#endif
