/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "panelElements.h"

#include <extraGtkFunctions/gtk_toolPanelWidget.h>
#include <uiElements/ui_elements.h>

/**
 * SECTION: panelElements
 * @short_description: The tab where #VisuElement characteristics can
 * be tuned.
 *
 * <para>It is possible to get the list of selected elements by
 * calling visu_ui_panel_elements_getSelected().</para>
 */

/**
 * panelElements:
 *
 * A pointer on the #VisuUiPanel that host the elements chooser.
 */
static GtkWidget *panelElements = NULL;

/* Local methods. */
static void createInteriorElements(GtkWidget *visu_ui_panel);

/**
 * visu_ui_panel_elements_init: (skip)
 * @ui: a #VisuUiMain object.
 *
 * Should be used in the list declared in externalModules.h to be loaded by
 * V_Sim on start-up. This routine will create the #VisuUiPanel where the element
 * stuff can be done, such as choosing a colour, setting the radius of
 * spheres...
 *
 * Returns: a newly created #VisuUiPanel object.
 */
VisuUiPanel* visu_ui_panel_elements_init(VisuUiMain *ui _U_)
{
  gchar *cl = _("Set elements caracteristics");
  gchar *tl = _("Elements");

  panelElements = visu_ui_panel_newWithIconFromPath("Panel_elements", cl, tl,
                                                    "stock-elements_20.png");
  g_return_val_if_fail(panelElements, (VisuUiPanel*)0);

  createInteriorElements(panelElements);
  visu_ui_panel_setDockable(VISU_UI_PANEL(panelElements), TRUE);

  return VISU_UI_PANEL(panelElements);
}

static void createInteriorElements(GtkWidget *visu_ui_panel)
{
  GtkWidget *vbox;
  GtkWidget *scrollView;
  VisuGlNodeScene *scene;

  scrollView = gtk_scrolled_window_new((GtkAdjustment*)0,
				       (GtkAdjustment*)0);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrollView),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrollView), GTK_SHADOW_NONE);

  scene = visu_ui_rendering_window_getGlScene(visu_ui_main_class_getDefaultRendering());

  vbox = visu_ui_elements_new(VISU_NODE_ARRAY_RENDERER(visu_gl_node_scene_getNodes(scene)));
#if GTK_MAJOR_VERSION < 3 || (GTK_MAJOR_VERSION == 3 && GTK_MINOR_VERSION < 8)
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollView), vbox);
#else
  gtk_container_add(GTK_CONTAINER(scrollView), vbox);
#endif

  gtk_widget_show_all(scrollView);

  gtk_container_add(GTK_CONTAINER(visu_ui_panel), scrollView);
}

/**
 * visu_ui_panel_elements_getStatic:
 *
 * Creates and retrieves the #VisuUiPanel used to handle #VisuElement.
 * 
 * Since: 3.8
 *
 * Returns: (transfer none): a #VisuUiPanel object.
 **/
GtkWidget* visu_ui_panel_elements_getStatic()
{
  if (!panelElements)
    visu_ui_panel_elements_init((VisuUiMain*)0);
  return panelElements;
}
