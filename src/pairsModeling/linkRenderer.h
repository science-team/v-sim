/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016-2017)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016-2017)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef LINK_RENDERER_H
#define LINK_RENDERER_H

#include <glib.h>
#include <glib-object.h>

#include "link.h"
#include <renderingMethods/elementRenderer.h>
#include <extraFunctions/colorizer.h>
#include <openGLFunctions/view.h>

G_BEGIN_DECLS

/* Boxed interface. */
#define VISU_TYPE_PAIR_LINK_RENDERER                (visu_pair_link_renderer_get_type())
#define VISU_PAIR_LINK_RENDERER(obj)                (G_TYPE_CHECK_INSTANCE_CAST((obj), VISU_TYPE_PAIR_LINK_RENDERER, VisuPairLinkRenderer))
#define VISU_PAIR_LINK_RENDERER_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_PAIR_LINK_RENDERER, VisuPairLinkRendererClass))
#define VISU_IS_PAIR_LINK_RENDERER(obj)             (G_TYPE_CHECK_INSTANCE_TYPE((obj), VISU_TYPE_PAIR_LINK_RENDERER))
#define VISU_IS_PAIR_LINK_RENDERER_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_PAIR_LINK_RENDERER))
#define VISU_PAIR_LINK_RENDERER_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_PAIR_LINK_RENDERER, VisuPairLinkRendererClass))

typedef struct _VisuPairLinkRenderer        VisuPairLinkRenderer;
typedef struct _VisuPairLinkRendererPrivate VisuPairLinkRendererPrivate;
typedef struct _VisuPairLinkRendererClass   VisuPairLinkRendererClass;

/**
 * VisuPairLinkRenderer:
 *
 * An opaque structure.
 */
struct _VisuPairLinkRenderer
{
  VisuObject parent;

  VisuPairLinkRendererPrivate *priv;
};

/**
 * VisuPairLinkRendererClass:
 * @parent: the parent class;
 * @start: a routine called before drawing every link defined by #VisuPairLink.
 * @stop: a routine called after drawing every link defined by #VisuPairLink.
 * @draw: a routine called for every link defined by #VisuPairLink.
 * @set_view: a routine called to set a different #VisuGlView object.
 *
 * The different routines common to objects implementing a #VisuPairLinkRenderer class.
 * 
 * Since: 3.8
 */
struct _VisuPairLinkRendererClass
{
  VisuObjectClass parent;

  void (*start)(VisuPairLinkRenderer *renderer, VisuPairLink *data,
                VisuElementRenderer *ele1, VisuElementRenderer *ele2,
                VisuDataColorizer *colorizer);
  void (*stop)(VisuPairLinkRenderer *renderer, VisuPairLink *data);
  void (*draw)(VisuPairLinkRenderer *renderer, const VisuPairLinkIter *iter);

  gboolean (*set_view)(VisuPairLinkRenderer *renderer, VisuGlView *view);
};

/**
 * visu_pair_link_renderer_get_type:
 *
 * This method returns the type of #VisuPairLinkRenderer, use
 * VISU_TYPE_PAIR_LINK_RENDERER instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuPairLinkRenderer.
 */
GType visu_pair_link_renderer_get_type(void);

void visu_pair_link_renderer_start(VisuPairLinkRenderer *renderer, VisuPairLink *data,
                                   VisuElementRenderer *ele1, VisuElementRenderer *ele2,
                                   VisuDataColorizer *colorizer);
void visu_pair_link_renderer_stop(VisuPairLinkRenderer *renderer, VisuPairLink *data);
void visu_pair_link_renderer_draw(VisuPairLinkRenderer *renderer,
                                  const VisuPairLinkIter *iter);

gboolean visu_pair_link_renderer_setGlView(VisuPairLinkRenderer *renderer, VisuGlView *view);

void visu_pair_link_renderer_emitDirty(VisuPairLinkRenderer *renderer);

G_END_DECLS

#endif
