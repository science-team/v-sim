/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "wire_renderer.h"
#include "iface_wire.h"
#include <coreTools/toolShade.h>

#include <math.h>
#include <GL/gl.h>

/**
 * SECTION:wire_renderer
 * @short_description: a class to render #VisuPairLink as wires.
 *
 * <para>This class is used to render #VisuPairLink as wires.</para>
 */

static void _start(VisuPairLinkRenderer *self, VisuPairLink *data,
                   VisuElementRenderer *ele1, VisuElementRenderer *ele2,
                   VisuDataColorizer *colorizer);
static void _stop(VisuPairLinkRenderer *self, VisuPairLink *data);
static void _draw(VisuPairLinkRenderer *self, const VisuPairLinkIter *iter);

G_DEFINE_TYPE(VisuPairWireRenderer, visu_pair_wire_renderer, VISU_TYPE_PAIR_LINK_RENDERER)

static void visu_pair_wire_renderer_class_init(VisuPairWireRendererClass *klass)
{
  DBG_fprintf(stderr, "Visu Wire Renderer: creating the class of the object.\n");

  /* Connect the overloading methods. */
  VISU_PAIR_LINK_RENDERER_CLASS(klass)->start = _start;
  VISU_PAIR_LINK_RENDERER_CLASS(klass)->stop = _stop;
  VISU_PAIR_LINK_RENDERER_CLASS(klass)->draw = _draw;
}
static void visu_pair_wire_renderer_init(VisuPairWireRenderer *obj _U_)
{
  DBG_fprintf(stderr, "Visu Wire Renderer: initializing a new object (%p).\n",
	      (gpointer)obj);
}
/**
 * visu_pair_wire_renderer_new:
 *
 * Creates a renderer to draw links as wires.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a newly created #VisuPairWireRenderer object.
 */
VisuPairWireRenderer* visu_pair_wire_renderer_new()
{
  return VISU_PAIR_WIRE_RENDERER(g_object_new(VISU_TYPE_PAIR_WIRE_RENDERER,
                                              "id", "Wire pairs",
                                              "label", _("Wire pairs"),
                                              "description", _("Pairs are rendered by flat lines."
                                                               " The color and the width can by chosen."), NULL));
}

static void _start(VisuPairLinkRenderer *self _U_, VisuPairLink *data,
                   VisuElementRenderer *ele1 _U_, VisuElementRenderer *ele2 _U_,
                   VisuDataColorizer *colorizer _U_)
{
  ToolColor *color;
  guint16 stipple;

  glLineWidth(visu_pair_wire_getWidth(VISU_PAIR_WIRE(data)));
  color = visu_pair_link_getColor(data);
  glColor3fv(color->rgba);
  stipple = visu_pair_wire_getStipple(VISU_PAIR_WIRE(data));
  if (stipple != 65535)
    {
      glEnable(GL_LINE_STIPPLE);
      glLineStipple(1, stipple);
    }
  glDisable(GL_LIGHTING);
  glDisable(GL_DITHER);
}

static void _stop(VisuPairLinkRenderer *self _U_, VisuPairLink *data _U_)
{
  glDisable(GL_LINE_STIPPLE);
  glEnable(GL_DITHER); /* WARNING: it is the default! */
  glEnable(GL_LIGHTING);
}

static void _draw(VisuPairLinkRenderer *self _U_, const VisuPairLinkIter *iter)
{
  float ratio;
  ToolColor *color;
  float rgba[4], mM[2];
  ToolShade *shade;

  color = visu_pair_link_getColor(iter->parent);
  shade = visu_pair_wire_getShade(VISU_PAIR_WIRE(iter->parent));
  if (shade)
    {
      mM[0] = visu_pair_link_getDistance(iter->parent, VISU_DISTANCE_MIN);
      mM[1] = visu_pair_link_getDistance(iter->parent, VISU_DISTANCE_MAX);
      ratio = (sqrt(iter->d2) - mM[0]) / (mM[1] - mM[0]);
      tool_shade_valueToRGB(shade, rgba, ratio);
      rgba[3] = iter->coeff * color->rgba[3];
      glColor4fv(rgba);
    }
  else
    glColor4f(color->rgba[0], color->rgba[1], color->rgba[2], iter->coeff * color->rgba[3]);

  glBegin(GL_LINES);
  glVertex3fv(iter->xyz1);
  if (iter->periodic)
    glVertex3f(iter->xyz1[0] + iter->dxyz[0] / 2.f,
               iter->xyz1[1] + iter->dxyz[1] / 2.f,
               iter->xyz1[2] + iter->dxyz[2] / 2.f);
  else
    glVertex3fv(iter->xyz2);
  glEnd();
  
  if (iter->periodic)
    {
      glBegin(GL_LINES);
      glVertex3fv(iter->xyz2);
      glVertex3f(iter->xyz2[0] - iter->dxyz[0] / 2.f,
                 iter->xyz2[1] - iter->dxyz[1] / 2.f,
                 iter->xyz2[2] - iter->dxyz[2] / 2.f);
      glEnd();
    }
}
