/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef OPENGL_H
#define OPENGL_H

#include <glib.h>
#include <glib-object.h>

#include "visu_tools.h"
#include "openGLFunctions/view.h"
#include "openGLFunctions/light.h"
#include "openGLFunctions/renderingMode.h"

G_BEGIN_DECLS

/**
 * VISU_TYPE_GL:
 *
 * return the type of #VisuGl.
 */
#define VISU_TYPE_GL	     (visu_gl_get_type ())
/**
 * VISU_GL:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuGl type.
 */
#define VISU_GL(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_GL, VisuGl))
/**
 * VISU_GL_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuGlClass.
 */
#define VISU_GL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_GL, VisuGlClass))
/**
 * VISU_IS_GL:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuGl object.
 */
#define VISU_IS_GL(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_GL))
/**
 * VISU_IS_GL_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuGlClass class.
 */
#define VISU_IS_GL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_GL))
/**
 * VISU_GL_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_GL_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_GL, VisuGlClass))

/**
 * VISU_GL_OFFSCREEN
 *
 * An hint mentioning that the OpenGL context is used for offscreen
 * rendering.
 *
 * Since: 3.8
 */
#define VISU_GL_OFFSCREEN 1 << 0

/**
 * VisuGlPrivate:
 * 
 * Private data for #VisuGl objects.
 */
typedef struct _VisuGlPrivate VisuGlPrivate;

/**
 * VisuGl:
 * 
 * Common name to refer to a #_VisuGl.
 */
typedef struct _VisuGl VisuGl;
struct _VisuGl
{
  VisuObject parent;

  VisuGlPrivate *priv;
};

/**
 * VisuGlClass:
 * @parent: private.
 * @initContext: called when used in a new OpenGL context.
 * 
 * Common name to refer to a #_VisuGlClass.
 */
typedef struct _VisuGlClass VisuGlClass;
struct _VisuGlClass
{
  VisuObjectClass parent;

  void (*initContext)(VisuGl* gl);
};

/**
 * visu_gl_get_type:
 *
 * This method returns the type of #VisuGl, use
 * VISU_TYPE_GL instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuGl.
 */
GType visu_gl_get_type(void);

void visu_gl_setColor(VisuGl *gl, const float material[5], const float rgba[4]);
void visu_gl_setHighlightColor(VisuGl *gl, const float material[5],
                               const float rgb[3], float alpha);

void visu_gl_drawArrow(VisuGl *gl, const gfloat origin[3], const gfloat vect[3]);

/* Objects to play with lights. */
VisuGlLights* visu_gl_getLights(VisuGl *gl);
void visu_gl_applyLights(VisuGl *gl);

gboolean visu_gl_setAntialias(VisuGl *gl, gboolean value);
gboolean visu_gl_getAntialias(const VisuGl *gl);

gboolean visu_gl_setImmediate(VisuGl *gl, gboolean value);
gboolean visu_gl_getImmediate(const VisuGl *gl);

gboolean visu_gl_setTrueTransparency(VisuGl *gl, gboolean status);
gboolean visu_gl_getTrueTransparency(const VisuGl *gl);

gboolean visu_gl_getStereoCapability(const VisuGl *gl);
gboolean visu_gl_setStereo(VisuGl *gl, gboolean status);
gboolean visu_gl_getStereo(const VisuGl *gl);
gboolean visu_gl_setStereoAngle(VisuGl *gl, float angle);
float visu_gl_getStereoAngle(const VisuGl *gl);

gboolean visu_gl_setMode(VisuGl *gl, VisuGlRenderingMode value);
VisuGlRenderingMode visu_gl_getMode(const VisuGl *gl);

guint visu_gl_addHint(VisuGl *gl, guint value);
guint visu_gl_getHint(VisuGl *gl);

/* redraw methods */
void visu_gl_initContext(VisuGl *gl);

G_END_DECLS

#endif
